<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_ipd".
 *
 * @property string $AN
 * @property string $IPD_DATE
 */
class ChargeIpd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_ipd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN'], 'integer'],
            [['IPD_DATE'], 'safe'],
            [['SEND_BY'], 'string', 'max' => 255],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'IPD_DATE' => 'Ipd  Date',
            'SEND_BY' => 'ผู้นำส่ง',
        ];
    }
}
