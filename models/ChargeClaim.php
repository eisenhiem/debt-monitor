<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_claim".
 *
 * @property string $AN
 * @property string $CLAIM_DATE
 * @property string $D_UPDATE
 * @property string $CLAIM_STATUS
 */
class ChargeClaim extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_claim';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN'], 'integer'],
            [['CLAIM_DATE', 'D_UPDATE'], 'safe'],
            [['CLAIM_STATUS'], 'string', 'max' => 1],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'CLAIM_DATE' => 'Claim  Date',
            'D_UPDATE' => 'D  Update',
            'CLAIM_STATUS' => 'Claim  Status',
        ];
    }
}
