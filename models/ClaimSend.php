<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "claim_send".
 *
 * @property int $claim_id
 * @property string $send_date
 * @property string $send_time
 * @property int|null $user_id
 * @property string|null $d_update
 */
class ClaimSend extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'claim_send';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['claim_id', 'send_date', 'send_time'], 'required'],
            [['claim_id', 'user_id'], 'integer'],
            [['send_date', 'd_update'], 'safe'],
            [['send_time'], 'string', 'max' => 5],
            [['claim_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'claim_id' => 'Claim ID',
            'send_date' => 'Send Date',
            'send_time' => 'Send Time',
            'user_id' => 'User ID',
            'd_update' => 'D Update',
        ];
    }
}
