<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Loanchart;

/**
 * LoanchartSearch represents the model behind the search form of `app\models\Loanchart`.
 */
class LoanchartSearch extends Loanchart
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['AN', 'BROUGTH_BY', 'BROUGTH_DATE', 'RETURN_BY', 'RETURN_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loanchart::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'BROUGTH_DATE' => $this->BROUGTH_DATE,
            'RETURN_DATE' => $this->RETURN_DATE,
        ]);

        $query->andFilterWhere(['like', 'AN', $this->AN])
            ->andFilterWhere(['like', 'BROUGTH_BY', $this->BROUGTH_BY])
            ->andFilterWhere(['like', 'RETURN_BY', $this->RETURN_BY]);

        return $dataProvider;
    }
}
