<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_dc".
 *
 * @property string $AN
 * @property string $DC_DATE
 * @property string $DCTYPE
 * @property string $D_UPDATE
 */
class ChargeDc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_dc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'DC_DATE', 'DCTYPE'], 'required'],
            [['AN'], 'integer'],
            [['DC_DATE', 'D_UPDATE'], 'safe'],
            [['DCTYPE'], 'string', 'max' => 1],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'DC_DATE' => 'D/C  Date',
            'DCTYPE' => 'ประเภทการจำหน่าย',
            'D_UPDATE' => 'วันที่ปรับปรุงข้อมูล',
        ];
    }
}
