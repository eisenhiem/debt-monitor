<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClaimOpd;

/**
 * ClaimOpdSearch represents the model behind the search form of `app\models\ClaimOpd`.
 */
class ClaimOpdSearch extends ClaimOpd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['claim_id', 'hn', 'status_id', 'user_id'], 'integer'],
            [['date_service', 'time_service', 'inscl', 'pttype', 'doctor_id', 'd_update'], 'safe'],
            [['claim_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClaimOpd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'claim_id' => $this->claim_id,
            'hn' => $this->hn,
            'date_service' => $this->date_service,
            'inscl' => $this->inscl,
            'claim_price' => $this->claim_price,
            'status_id' => $this->status_id,
            'user_id' => $this->user_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'time_service', $this->time_service])
            ->andFilterWhere(['like', 'pttype', $this->pttype])
            ->andFilterWhere(['like', 'doctor_id', $this->doctor_id]);

        return $dataProvider;
    }
}
