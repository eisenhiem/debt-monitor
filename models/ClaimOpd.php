<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "claim_opd".
 *
 * @property int $claim_id
 * @property int $hn
 * @property string $date_service
 * @property string $time_service
 * @property string|null $pttype
 * @property string|null $inscl
 * @property string|null $doctor_id
 * @property float|null $claim_price
 * @property int|null $status_id
 * @property int|null $user_id
 * @property string|null $d_update
 */
class ClaimOpd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const STATUS_ACTIVE = 1 ;  
    const STATUS_DEACTIVE = 0 ;  


    public static function tableName()
    {
        return 'claim_opd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn', 'date_service', 'time_service'], 'required'],
            [['hn', 'status_id', 'user_id'], 'integer'],
            [['date_service', 'd_update'], 'safe'],
            [['claim_price'], 'number'],
            [['time_service', 'doctor_id'], 'string', 'max' => 5],
            [['pttype'], 'string', 'max' => 2],
            [['inscl'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'claim_id' => 'Claim ID',
            'hn' => 'HN',
            'date_service' => 'วันที่',
            'time_service' => 'เวลา',
            'pttype' => 'สิทธิ์',
            'inscl' => 'ประเภท',
            'doctor_id' => 'แพทย์',
            'claim_price' => 'ค่าใช้จ่าย',
            'status_id' => 'สถานะ',
            'user_id' => 'ผู้บันทึก',
            'd_update' => 'D Update',
        ];
    }

            /**
     * @return \yii\db\ActiveQuery
     */
    public function getRep()
    {
        return $this->hasOne(ClaimRep::className(), ['claim_id' => 'claim_id']);
    }

    public function getRepStatus()
    {
        return $this->rep->rep_status;
    }

    public function getPtins()
    {
        return $this->hasOne(Pttype::className(), ['PTTYPE_ID' => 'pttype']);
    }

    public function getPttypeName()
    {
        return $this->ptins->PTTYPE_NAME;
    }
}
