<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CheckAudit;

/**
 * CheckAuditSearch represents the model behind the search form of `app\models\CheckAudit`.
 */
class CheckAuditSearch extends CheckAudit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'integer'],
            [['is_admit_by_phone', 'is_refer', 'is_consult_by_phone', 'is_sign', 'is_document__complete','is_admission_note', 'diagnosis', 'order_time', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CheckAudit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'is_admit_by_phone', $this->is_admit_by_phone])
            ->andFilterWhere(['like', 'is_refer', $this->is_refer])
            ->andFilterWhere(['like', 'is_consult_by_phone', $this->is_consult_by_phone])
            ->andFilterWhere(['like', 'is_sign', $this->is_sign])
            ->andFilterWhere(['like', 'is_document__complete', $this->is_document__complete])
            ->andFilterWhere(['like', 'is-admission_note', $this->is_admission_note])
            ->andFilterWhere(['like', 'diagnosis', $this->diagnosis])
            ->andFilterWhere(['like', 'order_time', $this->order_time]);

        return $dataProvider;
    }
}
