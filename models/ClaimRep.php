<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "claim_rep".
 *
 * @property int $claim_id
 * @property string $rep_date
 * @property string $rep_status
 * @property string|null $d_update
 */
class ClaimRep extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'claim_rep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['claim_id', 'rep_date', 'rep_status'], 'required'],
            [['claim_id'], 'integer'],
            [['rep_date', 'd_update'], 'safe'],
            [['rep_status'], 'string', 'max' => 10],
            [['claim_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'claim_id' => 'Claim ID',
            'rep_date' => 'Rep Date',
            'rep_status' => 'Rep Status',
            'd_update' => 'D Update',
        ];
    }

    public static function itemsAlias($key)
    {
        $items = [
            'status' => [
                'Approve' => 'Approve',
                'Deny' => 'Deny',
            ],
        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }

    public function getStatusName()
    {
        return ArrayHelper::getValue($this->getItemStatus(), $this->rep_status);
    }

}
