<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cstatus".
 *
 * @property string $CLAIM_STATUS
 * @property string $DETAIL
 */
class Cstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cstatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CLAIM_STATUS', 'DETAIL'], 'required'],
            [['CLAIM_STATUS'], 'string', 'max' => 1],
            [['DETAIL'], 'string', 'max' => 10],
            [['CLAIM_STATUS'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CLAIM_STATUS' => 'Claim  Status',
            'DETAIL' => 'Detail',
        ];
    }
}
