<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "line".
 *
 * @property int $line_id
 * @property string|null $line_group
 * @property string|null $line_token
 * @property string|null $line_status
 */
class Line extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'line';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['line_token', 'line_status'], 'string'],
            [['line_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'line_id' => 'Line ID',
            'line_group' => 'Line Group',
            'line_token' => 'Line Token',
            'line_status' => 'Line Status',
        ];
    }
}
