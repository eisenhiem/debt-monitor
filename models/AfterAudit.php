<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "after_audit".
 *
 * @property int $AN
 * @property string $PRINCIPLE
 * @property string $COMOBIT
 * @property string $COMPLICATION
 * @property string $OTHER
 * @property string $EXTERNAL
 * @property double $ADJRW
 * @property int $AUDITER_ID
 * @property string $COMMENT
 * @property string $AUDIT_DATE
 */
class AfterAudit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'after_audit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'PRINCIPLE', 'ADJRW'], 'required'],
            [['AN', 'AUDITER_ID'], 'integer'],
            [['ADJRW'], 'number'],
            [['COMMENT'], 'string'],
            [['AUDIT_DATE'], 'safe'],
            [['PRINCIPLE'], 'string', 'max' => 7],
            [['COMOBIT', 'COMPLICATION', 'OTHER', 'EXTERNAL'], 'string', 'max' => 30],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'AN',
            'PRINCIPLE' => 'โรคหลัก',
            'COMOBIT' => 'โรคร่วม',
            'COMPLICATION' => 'โรคแทรก',
            'OTHER' => 'อื่นๆ',
            'EXTERNAL' => 'สาเหตุภายนอก',
            'ADJRW' => 'Adjrw',
            'AUDITER_ID' => 'Auditer',
            'COMMENT' => 'หมายเหตุ',
            'AUDIT_DATE' => 'วันที่ Audit',
        ];
    }

    public function getDoctor(){
        return $this->hasOne(Doctors::className(), ['AUDITER_ID' => 'DOCTOR_ID']);
    }

    public function getDoctorName(){
        $model=$this->doctor;
        return $model?$model->DOCTOR_NAME:'';
    }
    
}
