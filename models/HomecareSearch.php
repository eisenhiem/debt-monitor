<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Homecare;

/**
 * HomecareSearch represents the model behind the search form of `app\models\Homecare`.
 */
class HomecareSearch extends Homecare
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'HN'], 'integer'],
            [['location', 'selfcare', 'improve', 'not_improve', 'cg_sometime', 'cg_ontime', 'cg_name', 'cg_relation', 'hosp_treat', 'treat_summary', 'pt_problem', 'careplan', 'nurse'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Homecare::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'HN' => $this->HN,
        ]);

        $query->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'selfcare', $this->selfcare])
            ->andFilterWhere(['like', 'improve', $this->improve])
            ->andFilterWhere(['like', 'not_improve', $this->not_improve])
            ->andFilterWhere(['like', 'cg_sometime', $this->cg_sometime])
            ->andFilterWhere(['like', 'cg_ontime', $this->cg_ontime])
            ->andFilterWhere(['like', 'cg_name', $this->cg_name])
            ->andFilterWhere(['like', 'cg_relation', $this->cg_relation])
            ->andFilterWhere(['like', 'hosp_treat', $this->hosp_treat])
            ->andFilterWhere(['like', 'treat_summary', $this->treat_summary])
            ->andFilterWhere(['like', 'pt_problem', $this->pt_problem])
            ->andFilterWhere(['like', 'careplan', $this->careplan])
            ->andFilterWhere(['like', 'nurse', $this->nurse]);

        return $dataProvider;
    }
}
