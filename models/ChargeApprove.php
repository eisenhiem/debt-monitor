<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_approve".
 *
 * @property string $AN
 * @property string $APPROVE_DATE
 * @property string $D_UPDATE
 */
class ChargeApprove extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_approve';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN'], 'integer'],
            [['APPROVE_DATE', 'D_UPDATE'], 'safe'],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'AN',
            'APPROVE_DATE' => 'วันที่อนุมัติ',
            'D_UPDATE' => 'วันที่ปรับปรุงข้อมูล',
        ];
    }
}
