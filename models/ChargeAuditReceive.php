<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_admit".
 *
 * @property string $AN
 * @property string $D_UPDATE
 */
class ChargeAuditReceive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_audit_receive';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN'], 'integer'],
            [['D_UPDATE'], 'safe'],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'D_UPDATE' => 'Receive Date',
        ];
    }
}
