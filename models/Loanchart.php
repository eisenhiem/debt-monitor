<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "loanchart".
 *
 * @property string $id
 * @property string $AN
 * @property string $BROUGTH_BY
 * @property string $BROUGTH_DATE
 * @property string $RETURN_BY
 * @property string $RETURN_DATE
 */
class Loanchart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loanchart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BROUGTH_DATE', 'RETURN_DATE'], 'safe'],
            [['AN'], 'string', 'max' => 8],
            [['BROUGTH_BY', 'RETURN_BY'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'AN' => 'AN',
            'BROUGTH_BY' => 'ยืมโดย',
            'BROUGTH_DATE' => 'วันยืม',
            'RETURN_BY' => 'คืนโดย',
            'RETURN_DATE' => 'วันคืน',
        ];
    }
}
