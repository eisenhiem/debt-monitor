<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_receive".
 *
 * @property string $AN
 * @property string $RECEIVE_DATE
 * @property string $D_UPDATE
 * @property int $RECEIVER
 */
class ChargeReceive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_receive';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN', 'RECEIVER'], 'integer'],
            [['RECEIVE_DATE', 'D_UPDATE'], 'safe'],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'AN',
            'RECEIVE_DATE' => 'วันที่รับชาร์ตจากแพทย์',
            'D_UPDATE' => 'วันที่ปรับปรุงข้อมูล',
            'RECEIVER' => 'ผู้รับชาร์ต',
        ];
    }
}
