<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "appearl".
 *
 * @property int $appearl_id
 * @property int $claim_id
 * @property string $appearl_date
 * @property int|null $user_id
 * @property string|null $d_update
 */
class Appearl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appearl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['claim_id', 'appearl_date'], 'required'],
            [['claim_id', 'user_id'], 'integer'],
            [['appearl_date', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'appearl_id' => 'Appearl ID',
            'claim_id' => 'Claim ID',
            'appearl_date' => 'Appearl Date',
            'user_id' => 'User ID',
            'd_update' => 'D Update',
        ];
    }
}
