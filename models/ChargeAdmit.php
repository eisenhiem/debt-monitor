<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_admit".
 *
 * @property string $AN
 * @property string $ADMIT_DATE
 */
class ChargeAdmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_admit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'ADMIT_DATE'], 'required'],
            [['AN'], 'integer'],
            [['ADMIT_DATE'], 'safe'],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'ADMIT_DATE' => 'Admit  Date',
        ];
    }
}
