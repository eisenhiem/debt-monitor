<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Line;

/**
 * LineSearch represents the model behind the search form of `app\models\Line`.
 */
class LineSearch extends Line
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['line_id'], 'integer'],
            [['line_group', 'line_token', 'line_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Line::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'line_id' => $this->line_id,
        ]);

        $query->andFilterWhere(['like', 'line_group', $this->line_group])
            ->andFilterWhere(['like', 'line_token', $this->line_token])
            ->andFilterWhere(['like', 'line_status', $this->line_status]);

        return $dataProvider;
    }
}
