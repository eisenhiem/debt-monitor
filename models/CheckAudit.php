<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "check_audit".
 *
 * @property int $AN
 * @property string|null $is_admit_by_phone
 * @property string|null $is_refer
 * @property string|null $is_consult_by_phone
 * @property string|null $is_sign
 * @property string|null $is_document__complete
 * @property string|null $diagnosis
 * @property string|null $order_time
 * @property string|null $d_update
 */
class CheckAudit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'check_audit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN'], 'integer'],
            [['d_update'], 'safe'],
            [['is_admit_by_phone', 'is_refer', 'is_consult_by_phone', 'is_sign', 'is_document__complete','is_admission_note'], 'string', 'max' => 1],
            [['diagnosis', 'order_time'], 'string', 'max' => 255],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'is_admit_by_phone' => 'Admit รคส.',
            'is_refer' => 'ทรุดลง/ส่งต่อ',
            'is_consult_by_phone' => 'Consult ทางโทรศัพท์',
            'is_sign' => 'มีการลงชื่อใน order',
            'is_document__complete' => 'เอกสารประกอบครบ',
            'is_admission_note' => 'Admission Note',
            'diagnosis' => 'การวินิจฉัยโรค',
            'order_time' => 'เวลาที่สั่ง admit',
            'd_update' => 'D Update',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CheckAuditQuery the active query used by this AR class.
     */
    public function getResult($answer)
    {
        if($answer == '1'){
            return 'ใช่';
        }else{
            return 'ไม่ใช่';
        }
    }
}
