<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AfterAudit;

/**
 * AfterAuditSearch represents the model behind the search form of `app\models\AfterAudit`.
 */
class AfterAuditSearch extends AfterAudit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'integer'],
            [['PDX', 'SDX', 'AUDITER', 'DOCUMENT_REF', 'COMMENT', 'AUDIT_DATE'], 'safe'],
            [['ADJRW'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AfterAudit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'ADJRW' => $this->ADJRW,
            'AUDIT_DATE' => $this->AUDIT_DATE,
        ]);

        $query->andFilterWhere(['like', 'PDX', $this->PDX])
            ->andFilterWhere(['like', 'SDX', $this->SDX])
            ->andFilterWhere(['like', 'AUDITER', $this->AUDITER])
            ->andFilterWhere(['like', 'DOCUMENT_REF', $this->DOCUMENT_REF])
            ->andFilterWhere(['like', 'COMMENT', $this->COMMENT]);

        return $dataProvider;
    }
}
