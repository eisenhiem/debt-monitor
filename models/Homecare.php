<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "homecare".
 *
 * @property int $AN
 * @property int $HN
 * @property string $location
 * @property string $selfcare
 * @property string $improve
 * @property string $not_improve
 * @property string $cg_sometime
 * @property string $cg_ontime
 * @property string $cg_name
 * @property string $cg_relation
 * @property string $hosp_treat
 * @property string $treat_summary
 * @property string $pt_problem
 * @property string $careplan
 * @property string $nurse
 */
class Homecare extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homecare';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN', 'HN'], 'integer'],
            [['hosp_treat', 'treat_summary', 'pt_problem', 'careplan'], 'string'],
            [['location'], 'string', 'max' => 255],
            [['selfcare', 'improve', 'not_improve', 'cg_sometime', 'cg_ontime'], 'string', 'max' => 1],
            [['cg_name', 'nurse'], 'string', 'max' => 50],
            [['cg_relation'], 'string', 'max' => 30],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'AH',
            'HN' => 'HN',
            'location' => 'สถานที่ใกล้เคียง/แผนที่',
            'selfcare' => 'หายดูแลตัวเอง',
            'improve' => 'ทุเลา',
            'not_improve' => 'ไม่ดีขึ้น',
            'cg_sometime' => 'ต้องการผู้ช่วยดูแลบ้าง',
            'cg_ontime' => 'ต้องการผู้ช่วยดูแลตลอดเวลา',
            'cg_name' => 'ชื่อผู้ดูแล',
            'cg_relation' => 'ความเกี่ยวข้อง',
            'hosp_treat' => 'การรักษาใน รพ.',
            'treat_summary' => 'สรุปการให้การรักษาพยบาลที่ผ่านมา และอาการสำคัญก่อนจำหน่าย',
            'pt_problem' => 'ปัญหาและความต้องการผู้ป่วย',
            'careplan' => 'แผนการดูแลต่อเนื่อง',
            'nurse' => 'พยาบาล',
        ];
    }
}
