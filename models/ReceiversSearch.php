<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Receivers;

/**
 * ReceiversSearch represents the model behind the search form of `app\models\Receivers`.
 */
class ReceiversSearch extends Receivers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RECEIVER_ID'], 'integer'],
            [['RECEIVER_NAME'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Receivers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'RECEIVER_ID' => $this->RECEIVER_ID,
        ]);

        $query->andFilterWhere(['like', 'RECEIVER_NAME', $this->RECEIVER_NAME]);

        return $dataProvider;
    }
}
