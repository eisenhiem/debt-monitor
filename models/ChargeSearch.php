<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Charge;

/**
 * ChargeSearch represents the model behind the search form of `app\models\Charge`.
 */
class ChargeSearch extends Charge
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'DOCTOR_ID'], 'integer'],
            [['PTTYPE_ID', 'STATUS_ID', 'D_UPDATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Charge::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'DOCTOR_ID' => $this->DOCTOR_ID,
            'D_UPDATE' => $this->D_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'PTTYPE_ID', $this->PTTYPE_ID])
            ->andFilterWhere(['like', 'STATUS_ID', $this->STATUS_ID]);

        return $dataProvider;
    }
}
