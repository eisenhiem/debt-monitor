<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "receivers".
 *
 * @property string $RECEIVER_ID
 * @property string $RECEIVER_NAME
 */
class Receivers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'receivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RECEIVER_NAME'], 'required'],
            [['RECEIVER_NAME'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'RECEIVER_ID' => 'ID',
            'RECEIVER_NAME' => 'ชื่อ-สกุลผู้รับชาร์ต',
        ];
    }
}
