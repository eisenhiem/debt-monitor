/*
Navicat MySQL Data Transfer

Source Server         : hi_server
Source Server Version : 50568
Source Host           : 192.168.111.5:3306
Source Database       : chart

Target Server Type    : MYSQL
Target Server Version : 50568
File Encoding         : 65001

Date: 2022-06-07 15:50:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `after_audit`
-- ----------------------------
DROP TABLE IF EXISTS `after_audit`;
CREATE TABLE `after_audit` (
  `AN` int(8) NOT NULL,
  `PRINCIPLE` varchar(7) NOT NULL,
  `COMOBIT` varchar(30) DEFAULT NULL,
  `COMPLICATION` varchar(30) DEFAULT NULL,
  `OTHER` varchar(30) DEFAULT NULL,
  `EXTERNAL` varchar(30) DEFAULT NULL,
  `ADJRW` double(10,4) NOT NULL,
  `AUDITER_ID` int(5) DEFAULT NULL,
  `COMMENT` text,
  `AUDIT_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of after_audit
-- ----------------------------

-- ----------------------------
-- Table structure for `appearl`
-- ----------------------------
DROP TABLE IF EXISTS `appearl`;
CREATE TABLE `appearl` (
  `appearl_id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `appearl_date` date NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`appearl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appearl
-- ----------------------------

-- ----------------------------
-- Table structure for `before_audit`
-- ----------------------------
DROP TABLE IF EXISTS `before_audit`;
CREATE TABLE `before_audit` (
  `AN` int(8) NOT NULL,
  `PRINCIPLE` varchar(7) NOT NULL,
  `COMOBIT` varchar(30) DEFAULT NULL,
  `COMPLICATION` varchar(30) DEFAULT NULL,
  `OTHER` varchar(30) DEFAULT NULL,
  `EXTERNAL` varchar(30) DEFAULT NULL,
  `ADJRW` double(10,4) NOT NULL,
  `DOCUMENT_REF` text,
  `COMMENT` text,
  `SUMMARY_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of before_audit
-- ----------------------------

-- ----------------------------
-- Table structure for `charge`
-- ----------------------------
DROP TABLE IF EXISTS `charge`;
CREATE TABLE `charge` (
  `AN` int(8) unsigned NOT NULL,
  `PTTYPE_ID` varchar(2) NOT NULL,
  `DOCTOR_ID` int(5) NOT NULL,
  `STATUS_ID` varchar(1) NOT NULL DEFAULT '1',
  `D_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_admit`
-- ----------------------------
DROP TABLE IF EXISTS `charge_admit`;
CREATE TABLE `charge_admit` (
  `AN` int(8) unsigned NOT NULL,
  `ADMIT_DATE` datetime NOT NULL,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_admit
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_approve`
-- ----------------------------
DROP TABLE IF EXISTS `charge_approve`;
CREATE TABLE `charge_approve` (
  `AN` int(8) unsigned NOT NULL,
  `APPROVE_DATE` datetime DEFAULT NULL,
  `D_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_approve
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_claim`
-- ----------------------------
DROP TABLE IF EXISTS `charge_claim`;
CREATE TABLE `charge_claim` (
  `AN` int(8) unsigned NOT NULL,
  `CLAIM_DATE` datetime DEFAULT NULL,
  `D_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CLAIM_STATUS` varchar(1) NOT NULL DEFAULT 'P',
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_claim
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_dc`
-- ----------------------------
DROP TABLE IF EXISTS `charge_dc`;
CREATE TABLE `charge_dc` (
  `AN` int(8) unsigned NOT NULL,
  `DC_DATE` datetime NOT NULL,
  `DCTYPE` varchar(1) NOT NULL,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_dc
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_fu`
-- ----------------------------
DROP TABLE IF EXISTS `charge_fu`;
CREATE TABLE `charge_fu` (
  `AN` int(8) NOT NULL,
  `FU_DATE` datetime DEFAULT NULL,
  `HN` int(11) NOT NULL,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_fu
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_ipd`
-- ----------------------------
DROP TABLE IF EXISTS `charge_ipd`;
CREATE TABLE `charge_ipd` (
  `AN` int(8) unsigned NOT NULL,
  `IPD_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_ipd
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_receive`
-- ----------------------------
DROP TABLE IF EXISTS `charge_receive`;
CREATE TABLE `charge_receive` (
  `AN` int(8) unsigned NOT NULL,
  `RECEIVE_DATE` datetime DEFAULT NULL,
  `D_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RECEIVER` int(3) DEFAULT NULL,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_receive
-- ----------------------------

-- ----------------------------
-- Table structure for `charge_summary`
-- ----------------------------
DROP TABLE IF EXISTS `charge_summary`;
CREATE TABLE `charge_summary` (
  `AN` int(8) unsigned NOT NULL,
  `SEND_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of charge_summary
-- ----------------------------

-- ----------------------------
-- Table structure for `claim_opd`
-- ----------------------------
DROP TABLE IF EXISTS `claim_opd`;
CREATE TABLE `claim_opd` (
  `claim_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hn` int(11) NOT NULL,
  `date_service` date NOT NULL,
  `time_service` varchar(5) NOT NULL,
  `pttype` varchar(2) DEFAULT NULL,
  `inscl` varchar(3) DEFAULT NULL,
  `doctor_id` varchar(5) DEFAULT NULL,
  `claim_price` decimal(10,2) DEFAULT NULL,
  `status_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of claim_opd
-- ----------------------------

-- ----------------------------
-- Table structure for `claim_rep`
-- ----------------------------
DROP TABLE IF EXISTS `claim_rep`;
CREATE TABLE `claim_rep` (
  `claim_id` int(11) NOT NULL,
  `rep_date` date NOT NULL,
  `rep_status` varchar(10) NOT NULL,
  `d_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of claim_rep
-- ----------------------------

-- ----------------------------
-- Table structure for `claim_send`
-- ----------------------------
DROP TABLE IF EXISTS `claim_send`;
CREATE TABLE `claim_send` (
  `claim_id` int(11) NOT NULL,
  `send_date` date NOT NULL,
  `send_time` varchar(5) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of claim_send
-- ----------------------------

-- ----------------------------
-- Table structure for `cstatus`
-- ----------------------------
DROP TABLE IF EXISTS `cstatus`;
CREATE TABLE `cstatus` (
  `CLAIM_STATUS` varchar(1) NOT NULL,
  `DETAIL` varchar(10) NOT NULL,
  PRIMARY KEY (`CLAIM_STATUS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cstatus
-- ----------------------------
INSERT INTO `cstatus` VALUES ('A', 'ผ่าน');
INSERT INTO `cstatus` VALUES ('C', 'ติด C');
INSERT INTO `cstatus` VALUES ('P', 'รอตอบกลับ');

-- ----------------------------
-- Table structure for `dctype`
-- ----------------------------
DROP TABLE IF EXISTS `dctype`;
CREATE TABLE `dctype` (
  `DCTYPE_ID` int(1) unsigned NOT NULL,
  `DC_DETAIL` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`DCTYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dctype
-- ----------------------------

-- ----------------------------
-- Table structure for `doctors`
-- ----------------------------
DROP TABLE IF EXISTS `doctors`;
CREATE TABLE `doctors` (
  `DOCTOR_ID` int(5) NOT NULL,
  `DOCTOR_NAME` varchar(100) NOT NULL,
  `STATUS` varchar(1) NOT NULL DEFAULT '1',
  `AUDITER` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DOCTOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctors
-- ----------------------------

-- ----------------------------
-- Table structure for `homecare`
-- ----------------------------
DROP TABLE IF EXISTS `homecare`;
CREATE TABLE `homecare` (
  `AN` int(11) NOT NULL DEFAULT '0',
  `HN` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `selfcare` varchar(1) DEFAULT NULL,
  `improve` varchar(1) DEFAULT NULL,
  `not_improve` varchar(1) DEFAULT NULL,
  `cg_sometime` varchar(1) DEFAULT NULL,
  `cg_ontime` varchar(1) DEFAULT NULL,
  `cg_name` varchar(50) DEFAULT NULL,
  `cg_relation` varchar(30) DEFAULT NULL,
  `hosp_treat` text,
  `treat_summary` text,
  `pt_problem` text,
  `careplan` text,
  `nurse` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of homecare
-- ----------------------------

-- ----------------------------
-- Table structure for `loanchart`
-- ----------------------------
DROP TABLE IF EXISTS `loanchart`;
CREATE TABLE `loanchart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AN` varchar(8) DEFAULT NULL,
  `BROUGTH_BY` varchar(50) DEFAULT NULL,
  `BROUGTH_DATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `RETURN_BY` varchar(50) DEFAULT NULL,
  `RETURN_DATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of loanchart
-- ----------------------------

-- ----------------------------
-- Table structure for `profile`
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `sub_department_id` int(11) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `cid` varchar(13) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `position_level` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profile
-- ----------------------------
INSERT INTO `profile` VALUES ('1', '11', '37', 'นายจักรพงษ์ วงศ์กมลาไสย', '', 'นักวิชาการคอมพิวเตอร์ปฏิบัติการ', '');

-- ----------------------------
-- Table structure for `pttype`
-- ----------------------------
DROP TABLE IF EXISTS `pttype`;
CREATE TABLE `pttype` (
  `PTTYPE_ID` varchar(2) NOT NULL,
  `PTTYPE_NAME` varchar(50) NOT NULL,
  `STATUS` varchar(1) NOT NULL DEFAULT '1',
  `inscl` varchar(3) DEFAULT NULL,
  `income_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PTTYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pttype
-- ----------------------------

-- ----------------------------
-- Table structure for `receivers`
-- ----------------------------
DROP TABLE IF EXISTS `receivers`;
CREATE TABLE `receivers` (
  `RECEIVER_ID` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `RECEIVER_NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`RECEIVER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of receivers
-- ----------------------------

-- ----------------------------
-- Table structure for `status`
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `STATUS_ID` varchar(1) NOT NULL,
  `DETAIL` varchar(20) NOT NULL,
  `WORKING_TIME` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`STATUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of status
-- ----------------------------
INSERT INTO `status` VALUES ('0', 'Admit',0);
INSERT INTO `status` VALUES ('1', 'Discharge',0);
INSERT INTO `status` VALUES ('2', 'IPD สรุปค่าใช้จ่าย',0);
INSERT INTO `status` VALUES ('3', 'แพทย์สรุปชาร์ต',0);
INSERT INTO `status` VALUES ('4', 'ให้รหัส ICD10',0);
INSERT INTO `status` VALUES ('5', 'ส่ง E-Claim',0);
INSERT INTO `status` VALUES ('6', 'ตอบรับกลับจากสปสช.',0);

-- ----------------------------
-- Table structure for `token`
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`) USING BTREE,
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES ('1', 'ycfHswUqdZuUh748i0C0Dstqt_KfctWC', '1519701696', '0');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `last_login_at` int(11) NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `role` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_email` (`email`) USING BTREE,
  UNIQUE KEY `user_unique_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'jwongkamalasai@gmail.com', '$2y$12$/5wZnVSltPCI8XSqVjpU2.L1IZGV.zwbTUaHdA9zIm7u0OK9N8YX2', 'psQonEtwnqCrhKDcYghEKw_QaVj0FO9G', null, null, null, '192.168.111.3', '1531186376', '1531186376', '1654583781', '0', '1');

-- ----------------------------
-- View structure for `admit`
-- ----------------------------
DROP VIEW IF EXISTS `admit`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `admit` AS select `i`.`an` AS `an`,`i`.`rgtdate` AS `rgtdate`,`i`.`dchdate` AS `dchdate`,`i`.`daycnt` AS `daycnt`,`x`.`icd10` AS `icd10`,if((`x`.`icd10name` <> ''),`x`.`icd10name`,(select `hi`.`icd101`.`icd10name` from `hi`.`icd101` where (`hi`.`icd101`.`icd10` = `x`.`icd10`) limit 1)) AS `lastdx` from (`hi`.`ipt` `i` join `hi`.`iptdx` `x` on(((`i`.`an` = `x`.`an`) and (`x`.`itemno` = 1)))) ;

-- ----------------------------
-- View structure for `month_report`
-- ----------------------------
DROP VIEW IF EXISTS `month_report`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `month_report` AS select count(`a`.`AN`) AS `admits`,count(`d`.`AN`) AS `DC`,count(`r`.`AN`) AS `recieve`,count(`s`.`AN`) AS `summary_chart`,count(`e`.`AN`) AS `claim`,count(`p`.`AN`) AS `rep`,count((case `e`.`CLAIM_STATUS` when 'C' then `e`.`AN` end)) AS `C` from ((((((`charge` `c` left join `charge_admit` `a` on(((`c`.`AN` = `a`.`AN`) and (date_format(`a`.`ADMIT_DATE`,'%Y-%m') = date_format(now(),'%Y-%m'))))) left join `charge_dc` `d` on(((`c`.`AN` = `d`.`AN`) and (date_format(`d`.`DC_DATE`,'%Y-%m') = date_format(now(),'%Y-%m'))))) left join `charge_receive` `r` on(((`c`.`AN` = `r`.`AN`) and (date_format(`r`.`D_UPDATE`,'%Y-%m') = date_format(now(),'%Y-%m'))))) left join `charge_summary` `s` on(((`c`.`AN` = `s`.`AN`) and (date_format(`s`.`SEND_DATE`,'%Y-%m') = date_format(now(),'%Y-%m'))))) left join `charge_claim` `e` on(((`c`.`AN` = `e`.`AN`) and (date_format(`e`.`D_UPDATE`,'%Y-%m') = date_format(now(),'%Y-%m'))))) left join `charge_approve` `p` on(((`c`.`AN` = `p`.`AN`) and (date_format(`p`.`D_UPDATE`,'%Y-%m') = date_format(now(),'%Y-%m'))))) ;

-- ----------------------------
-- View structure for `patient`
-- ----------------------------
DROP VIEW IF EXISTS `patient`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `patient` AS select `p`.`hn` AS `HN`,`p`.`fname` AS `fname`,`p`.`lname` AS `lname`,timestampdiff(YEAR,`p`.`brthdate`,now()) AS `age`,concat(`p`.`addrpart`,' ',`m`.`namemoob`) AS `namemoob`,`t`.`nametumb` AS `nametumb`,`a`.`nameampur` AS `nameampur`,`c`.`namechw` AS `namechw`,`p`.`hometel` AS `hometel` from ((((`hi`.`pt` `p` left join `hi`.`mooban` `m` on(((`p`.`moopart` = `m`.`moopart`) and (`p`.`tmbpart` = `m`.`tmbpart`) and (`p`.`amppart` = `m`.`amppart`) and (`p`.`chwpart` = `m`.`chwpart`)))) left join `hi`.`tumbon` `t` on(((`p`.`tmbpart` = `t`.`tmbpart`) and (`p`.`amppart` = `t`.`amppart`) and (`p`.`chwpart` = `t`.`chwpart`)))) left join `hi`.`ampur` `a` on(((`p`.`amppart` = `a`.`amppart`) and (`p`.`chwpart` = `a`.`chwpart`)))) left join `hi`.`changwat` `c` on((`p`.`chwpart` = `c`.`chwpart`))) ;

-- ----------------------------
-- View structure for `pi`
-- ----------------------------
DROP VIEW IF EXISTS `pi`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `pi` AS select `i`.`an` AS `an`,`i`.`hn` AS `hn`,group_concat(`pi`.`pillness` separator ',') AS `pi` from (`hi`.`ipt` `i` join `hi`.`pillness` `pi` on((`i`.`vn` = `pi`.`vn`))) group by `i`.`an` ;

-- ----------------------------
-- View structure for `report`
-- ----------------------------
DROP VIEW IF EXISTS `report`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `report` AS select `c`.`AN` AS `AN`,`c`.`PTTYPE_ID` AS `PTTYPE_ID`,`a`.`ADMIT_DATE` AS `admit`,`d`.`DC_DATE` AS `DC`,date_format(`d`.`DC_DATE`,'%Y-%m') AS `dc_month`,`i`.`IPD_DATE` AS `receive`,`r`.`D_UPDATE` AS `send_dr`,`s`.`SEND_DATE` AS `receive_dr`,`e`.`D_UPDATE` AS `claim`,`p`.`D_UPDATE` AS `approve`,`b`.`PRINCIPLE` AS `PRINCIPLE`,`b`.`COMOBIT` AS `COMOBIT`,`b`.`COMPLICATION` AS `COMPLICATION`,`b`.`OTHER` AS `OTHER`,`b`.`EXTERNAL` AS `EXTERNAL`,`b`.`ADJRW` AS `ADJRW`,`f`.`PRINCIPLE` AS `after_Pdx`,`f`.`COMOBIT` AS `after_Comob`,`f`.`COMPLICATION` AS `after_comp`,`f`.`OTHER` AS `after_other`,`f`.`EXTERNAL` AS `after_ext`,`f`.`ADJRW` AS `after_adjRW`,concat(`dr`.`fname`,' ',`dr`.`lname`) AS `dr_name`,`c`.`DOCTOR_ID` AS `DOCTOR_ID` from ((((((((((`chart`.`charge` `c` left join `chart`.`charge_admit` `a` on((`c`.`AN` = `a`.`AN`))) left join `chart`.`charge_dc` `d` on((`c`.`AN` = `d`.`AN`))) left join `chart`.`charge_ipd` `i` on((`c`.`AN` = `i`.`AN`))) left join `chart`.`charge_receive` `r` on((`c`.`AN` = `r`.`AN`))) left join `chart`.`charge_summary` `s` on((`c`.`AN` = `s`.`AN`))) left join `chart`.`charge_claim` `e` on((`c`.`AN` = `e`.`AN`))) left join `chart`.`charge_approve` `p` on((`c`.`AN` = `p`.`AN`))) left join `chart`.`before_audit` `b` on((`c`.`AN` = `b`.`AN`))) left join `chart`.`after_audit` `f` on((`c`.`AN` = `f`.`AN`))) left join `hi`.`dct` `dr` on((`c`.`DOCTOR_ID` = `dr`.`lcno`))) ;

-- ----------------------------
-- View structure for `summary`
-- ----------------------------
DROP VIEW IF EXISTS `summary`;
CREATE ALGORITHM=UNDEFINED DEFINER=`chart`@`%` SQL SECURITY DEFINER VIEW `summary` AS select distinct `c`.`AN` AS `AN`,`c`.`DOCTOR_ID` AS `DOCTOR_ID`,`c`.`PTTYPE_ID` AS `PTTYPE_ID`,if(isnull(`a`.`AN`),0,1) AS `admit`,if(isnull(`d`.`AN`),0,1) AS `dc`,if(isnull(`i`.`AN`),0,1) AS `ipd`,if(isnull(`r`.`AN`),0,1) AS `receive`,if(isnull(`s`.`AN`),0,1) AS `dr_summary`,if(isnull(`e`.`AN`),0,`e`.`CLAIM_STATUS`) AS `eclaim`,if(isnull(`p`.`AN`),0,1) AS `approve`,`c`.`D_UPDATE` AS `D_UPDATE` from (((((((`charge` `c` left join `charge_admit` `a` on((`c`.`AN` = `a`.`AN`))) left join `charge_dc` `d` on((`c`.`AN` = `d`.`AN`))) left join `charge_summary` `s` on((`c`.`AN` = `s`.`AN`))) left join `charge_receive` `r` on((`c`.`AN` = `r`.`AN`))) left join `charge_claim` `e` on((`c`.`AN` = `e`.`AN`))) left join `charge_approve` `p` on((`c`.`AN` = `p`.`AN`))) left join `charge_ipd` `i` on((`c`.`AN` = `p`.`AN`))) ;

-- ----------------------------
-- Procedure structure for `chart_update`
-- ----------------------------
DROP PROCEDURE IF EXISTS `chart_update`;
DELIMITER ;;
CREATE DEFINER=`hiuser`@`%` PROCEDURE `chart_update`()
BEGIN

	insert ignore into chart.charge (`AN`,`PTTYPE_ID`,`DOCTOR_ID`,`STATUS_ID`)  
		select 
			i.an as AN,
			i.pttype as pttype_id,
			d.lcno as doctor_id,
			0 as status_id
		from hi.ipt as i 
		inner join hi.iptdx as x on i.an=x.an and x.itemno=1 
		inner join hi.dct as d on  (
		CASE WHEN LENGTH(x.dct) = 5 THEN d.lcno = x.dct 
			WHEN LENGTH(x.dct) = 4 THEN d.dct = substr(x.dct,3,2)  
			WHEN LENGTH(x.dct) = 2 THEN d.dct = x.dct END )
		where dchdate >= '2021-10-01' or rgtdate >= '2021-10-01'
;
	insert ignore into hi.charts (`an`,`pttype_code`,`pttype_inscl`,`chart_owner`,`chart_status_id`)  
		select 
			i.an as AN,
			s.inscl as pttype_code,
			(case s.inscl 
				when 'UCS' then s.instypeold
				when 'SSI' then s.instypeold
				when 'SSS' then s.ins_code
				else s.stdcode
			end ) as pttype_inscl,
			d.lcno as chart_owner,
			1 as chart_status_id
		from hi.ipt as i 
		inner join hi.iptdx as x on i.an=x.an and x.itemno=1 
		inner join hi.pttype as s on i.pttype = s. pttype
		inner join hi.dct as d on  (
		CASE WHEN LENGTH(x.dct) = 5 THEN d.lcno = x.dct 
			WHEN LENGTH(x.dct) = 4 THEN d.dct = substr(x.dct,3,2)  
			WHEN LENGTH(x.dct) = 2 THEN d.dct = x.dct END )
		where dchdate >= '2021-10-01' or rgtdate >= '2021-10-01'
;

insert ignore into chart.charge_admit 
		select 
			i.an as AN,
			concat(i.rgtdate,' ',time(i.rgttime*100)) as ADMIT_DATE
		from hi.ipt as i 
		where dchdate >= '2021-10-01' or rgtdate >= '2021-10-01';

	insert ignore into chart.charge_dc 
		select 
			i.an as AN,
			concat(i.dchdate,' ',time(i.dchtime*100)) as DC_DATE,
			i.dchtype as DCTYPE
		from hi.ipt as i 
		where dchdate >= '2021-10-01' or rgtdate >= '2021-10-01' and dchdate != '0000-00-00'
	;

update charge set STATUS_ID = 1 where STATUS_ID = 0 and AN in (select AN from charge_dc);
update hi.charts as c 
inner join ipt as i on c.an=i.an and dchdate >= '2021-10-01' or rgtdate >= '2021-10-01' and dchdate != '0000-00-00' 
set chart_status_id = 2 where chart_status_id = 1;

insert ignore into chart.before_audit (`AN`,`PRINCIPLE`,`COMOBIT`,`COMPLICATION`,`OTHER`,`EXTERNAL`,`ADJRW`)
select 
i.an as an,
cast(GROUP_CONCAT(distinct if(itemno=1,x.icd10,null)) as char(7)) as pdx,
cast(GROUP_CONCAT(distinct if(itemno=2,x.icd10,null)) as char(30)) as Cdx,
cast(GROUP_CONCAT(distinct if(itemno=3,x.icd10,null)) as char(30)) as Cpdx,
cast(GROUP_CONCAT(distinct if(itemno=4,x.icd10,null)) as char(30)) as Odx,
cast(GROUP_CONCAT(distinct if(itemno=5,x.icd10,null)) as char(30)) as Xdx,
i.adjrw
from 
hi.ipt as i 
inner join hi.iptdx as x on i.an=x.an 
where dchdate >= '2021-10-01' and rgtdate >= '2021-10-01' and adjrw != 0
group by i.an
;

insert into chart.after_audit (`AN`,`PRINCIPLE`,`COMOBIT`,`COMPLICATION`,`OTHER`,`EXTERNAL`,`ADJRW`)
select * from 
(select 
i.an as an,
cast(GROUP_CONCAT(distinct if(itemno=1,x.icd10,null)) as char(7)) as pdx,
cast(GROUP_CONCAT(distinct if(itemno=2,x.icd10,null)) as char(30)) as Cdx,
cast(GROUP_CONCAT(distinct if(itemno=3,x.icd10,null)) as char(30)) as Cpdx,
cast(GROUP_CONCAT(distinct if(itemno=4,x.icd10,null)) as char(30)) as Odx,
cast(GROUP_CONCAT(distinct if(itemno=5,x.icd10,null)) as char(30)) as Xdx,
i.adjrw
from 
hi.ipt as i 
inner join hi.iptdx as x on i.an=x.an 
where dchdate >= '2021-10-01' and rgtdate >= '2021-10-01' and adjrw != 0 and i.an in (select AN from chart.charge_claim)
group by i.an ) as r 
ON DUPLICATE KEY UPDATE PRINCIPLE=r.pdx, COMOBIT=r.Cdx,COMPLICATION=r.Cpdx,OTHER=r.Odx,EXTERNAL=r.Xdx,ADJRW=r.adjrw 
;

insert ignore into chart.charge_fu (`AN`,`FU_DATE`,`HN`) 
select 
i.an,
a.fudate,
i.hn
from hi.ipt as i 
inner join hi.oapp as a on a.an=i.an
		where dchdate >= '2021-10-01' or rgtdate >= '2021-10-01' and dchdate != '0000-00-00'
group by i.an,a.fudate;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `update_opd`
-- ----------------------------
DROP PROCEDURE IF EXISTS `update_opd`;
DELIMITER ;;
CREATE DEFINER=`hiuser`@`%` PROCEDURE `update_opd`()
BEGIN

insert into chart.claim_opd (hn,date_service,time_service,pttype,inscl,doctor_id,claim_price,status_id,user_id) 
select 
hn,date(o.vstdttm),date_format(o.vstdttm,'%H:%i'),o.pttype,t.inscl,d.lcno,sum(i.rcptamt),0,1
from hi.ovst as o 
inner join hi.incoth as i on o.vn=i.vn and o.an=0
inner join hi.pttype as t on o.pttype =t.pttype 
left join hi.dct as d on (case length(o.dct) when 4 then substr(o.dct,3,2) = d.dct when 5 then o.dct=d.lcno end)
where date(o.vstdttm) = date(SUBDATE(now(),INTERVAL 1 day))
and t.pttype in ('19','24','29','31','34','36','37','40','86')
group by o.vn;

insert into chart.claim_opd (hn,date_service,time_service,pttype,inscl,doctor_id,claim_price,status_id,user_id) 
select 
hn,date(o.vstdttm),date_format(o.vstdttm,'%H:%i'),o.pttype,'ANC',d.lcno,sum(i.rcptamt),0,1
from hi.ovst as o 
inner join hi.incoth as i on o.vn=i.vn and o.an=0
inner join hi.pttype as t on o.pttype =t.pttype 
inner join hi.anc as a on o.vn=a.vn 
left join hi.dct as d on (case length(o.dct) when 4 then substr(o.dct,3,2) = d.dct when 5 then o.dct=d.lcno end)
where date(o.vstdttm) = date(SUBDATE(now(),INTERVAL 1 day))
and t.pttype in ('15')
group by o.vn;

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for `charge_update`
-- ----------------------------
DROP EVENT IF EXISTS `charge_update`;
DELIMITER ;;
CREATE DEFINER=`hiuser`@`%` EVENT `charge_update` ON SCHEDULE EVERY 4 HOUR STARTS '2022-06-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO call chart_update
;;
DELIMITER ;

-- ----------------------------
-- Event structure for `insert_opd`
-- ----------------------------
DROP EVENT IF EXISTS `insert_opd`;
DELIMITER ;;
CREATE DEFINER=`hiuser`@`%` EVENT `insert_opd` ON SCHEDULE EVERY 1 DAY STARTS '2022-06-08 03:00:00' ON COMPLETION NOT PRESERVE ENABLE DO call update_opd
;;
DELIMITER ;

-- ----------------------------
-- Insert Pttype 
-- ----------------------------
insert into chart.pttype 
select 
pttype,namepttype,1,inscl,income_id
from hi.pttype ;

-- ----------------------------
-- Insert Doctors
-- ----------------------------
insert into chart.doctors
select 
lcno,concat(fname,' ',lname),1,0
from hi.dct
where lcno <> '';

-- -----------------------------
-- Version 2.3 
-- Add Table Line Notify
-- -----------------------------
CREATE TABLE `line` (
  `line_id` int(11) NOT NULL AUTO_INCREMENT,
  `line_group` varchar(255) DEFAULT NULL,
  `line_token` text,
  `line_status` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`line_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Version 2.4
-- Add Table Check Audit
-- ----------------------------
CREATE TABLE `check_audit` (
  `AN` int(8) NOT NULL,
  `is_admission_note` varchar(1) DEFAULT '1',
  `is_admit_by_phone` varchar(1) DEFAULT '1',
  `is_refer` varchar(1) DEFAULT '1',
  `is_consult_by_phone` varchar(1) DEFAULT '1',
  `is_sign` varchar(1) DEFAULT '1',
  `is_document__complete` varchar(1) DEFAULT '1',
  `diagnosis` varchar(255) DEFAULT NULL,
  `order_time` varchar(255) DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Update Table charge_ipd
-- ----------------------------
ALTER TABLE `charge_ipd`
ADD Column 'SEND_BY' varchar(255) DEFAULT NULL AFTER 'IPD_DATE';
-- ----------------------------