<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Homecare */

$this->title = $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Homecares', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="homecare-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>        
        <?= Html::a('พิมพ์ใบวางแผนดูแล', ['pdf', 'id' => $model->AN], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->AN], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->AN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'AN',
            'HN',
            'location',
            'selfcare',
            'improve',
            'not_improve',
            'cg_sometime',
            'cg_ontime',
            'cg_name',
            'cg_relation',
            'hosp_treat:ntext',
            'treat_summary:ntext',
            'pt_problem:ntext',
            'careplan:ntext',
            'nurse',
        ],
    ]) ?>

</div>
