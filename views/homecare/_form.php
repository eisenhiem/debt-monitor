<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Homecare */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="homecare-form">
    <div class="from-input">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'AN')->textInput() ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'HN')->textInput() ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-2">
        <?= $form->field($model, 'selfcare')->checkbox() ?>
    </div>
    <div class="col-md-2">
    <?= $form->field($model, 'improve')->checkbox() ?>
    </div>
    <div class="col-md-2">
    <?= $form->field($model, 'cg_sometime')->checkbox() ?>
    </div>
    <div class="col-md-2">
    <?= $form->field($model, 'not_improve')->checkbox() ?>
    </div>
    <div class="col-md-4">
    <?= $form->field($model, 'cg_ontime')->checkbox() ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'cg_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'cg_relation')->textInput(['maxlength' => true]) ?>
    </div>
    </div>

    <?= $form->field($model, 'hosp_treat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'treat_summary')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pt_problem')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'careplan')->textarea(['rows' => 6]) ?>
    <div class="row">
    <div class="col-md-6">
    <?= $form->field($model, 'nurse')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
