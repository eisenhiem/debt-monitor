<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HomecareSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Homecares';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="homecare-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มแผนดูแลต่อเนื่อง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'AN',
            'HN',
            //'location',
            //'selfcare',
            //'improve',
            //'not_improve',
            //'cg_sometime',
            //'cg_ontime',
            //'cg_name',
            //'cg_relation',
            //'hosp_treat:ntext',
            //'treat_summary:ntext',
            //'pt_problem:ntext',
            //'careplan:ntext',
            //'nurse',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a('รายละเอียด',['view','id' => $model->AN], ['class'=>'btn btn-info']);
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a('แก้ไข',['update','id' => $model->AN], ['class'=>'btn btn-success']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
