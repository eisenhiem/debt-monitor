<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Charge */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charge-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-xs-8 offset-xs-2">
            <div class="card card-primary">
                <div class="card-title text-center bg-success">
                    <b>AN : <?= $model->AN ?></b>
                </div>
                <div class="card-body">
                    <?= $form->field($model, 'SEND_BY')->textInput() ?>
                </div>
                <div class="card-footer">
                    <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>