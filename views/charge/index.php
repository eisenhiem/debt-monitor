<?php

use app\models\ChargeClaim;
use app\models\CheckAudit;
use yii\helpers\Html;
use app\models\Pttype;
use app\models\Doctors;
use app\models\Status;

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChargeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เวชระเบียนผู้ป่วยใน';
const STATUS_ACTIVE = 1 ;  
const STATUS_DEACTIVE = 0 ;  
?>
<div class="charge-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => [
            'heading' => "รายการชาร์ต",
            'type' => GridView::TYPE_PRIMARY
          ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'AN',
                'contentOptions'=>['style'=>'width: 100px;'],
            ],
            //'PTTYPE_ID',
            [
                'attribute'=>'PTTYPE_ID',
                'filter'=>ArrayHelper::map(Pttype::find()->where(['STATUS'=>STATUS_ACTIVE])->asArray()->all(), 'PTTYPE_ID', 'PTTYPE_NAME'),
                'label'=>'สิทธิ์ผู้ป่วย',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getPttypeName();
                }
            ],
            [
                'attribute'=>'DOCTOR_ID',
                'filter'=>ArrayHelper::map(Doctors::find()->where(['STATUS'=>STATUS_ACTIVE])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME'),
                'label'=>'แพทย์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDoctorName();
                }
            ],
            //'DOCTOR_ID',
            [
                'attribute'=>'STATUS_ID',
                'filter'=>ArrayHelper::map(STATUS::find()->asArray()->all(), 'STATUS_ID', 'DETAIL'),
                'label'=>'สถานะ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getStatusName();
                }
            ],
            //'STATUS_ID',
            //'D_UPDATE',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template'=>'{send} {claim} {approve} {appearl} {ipd} {rep} {summary-receive} {audit} {audit-receive} {coding}',
                'contentOptions'=>['style'=>'width: 180px;'],
                'buttons'=>[
                    'ipd' => function($url,$model,$key){
                        return $model->STATUS_ID == '1' ? Html::a('รับชาร์ตจาก IPD',['ipd','id' => $model->AN],['class'=>'btn btn-danger btn-block']) : null ;
                    },
                    'send' => function($url,$model,$key){
                        return $model->STATUS_ID == '2' ? Html::a('ส่งแพทย์',['send','id' => $model->AN],['class'=>'btn btn-info btn-block']) : null ;
                    },
                    'summary-receive' => function($url,$model,$key){
                        return $model->STATUS_ID == '3' ? Html::a('รับสรุปชาร์ต',['summary-receive','id' => $model->AN],['class'=>'btn btn-primary btn-block']) : null ;
                    },
                    'audit' => function($url,$model,$key){
                        return $model->STATUS_ID == '4' ? Html::a('ส่ง Audit',['audit','id' => $model->AN],['class'=>'btn btn-primary btn-block']) : null ;
                    },
                    'audit-receive' => function($url,$model,$key){
                        return $model->STATUS_ID == '5' ? Html::a('รับชาร์ต Audit',['audit-receive','id' => $model->AN],['class'=>'btn btn-primary btn-block']) : null ;
                    },
                    'coding' => function($url,$model,$key){
                        return $model->STATUS_ID == '6' ? Html::a('ลง Coding',['coding','id' => $model->AN],['class'=>'btn btn-primary btn-block']) : null ;
                    },
                    'claim' => function($url,$model,$key){
                        return $model->STATUS_ID == '7' ? Html::a('ส่ง Claim',['claim','id' => $model->AN],['class'=>'btn btn-warning btn-block']) : null ;
                    },
                    'approve' => function($url,$model,$key){
                        return $model->STATUS_ID == '8' ? Html::a('Approve',['approve','id' => $model->AN],['class'=>'btn btn-success']) .' '. Html::a('Deny',['deny','id' => $model->AN],['class'=>'btn btn-danger']) : null ;
                    },
                    'appearl' => function($url,$model,$key){
                        $model2 = ChargeClaim::find()->where(['AN' => $model->AN,'CLAIM_STATUS'=>'C'])->one();
                        return $model2 ? Html::a('แก้ C',['approve','id' => $model->AN],['class'=>'btn btn-success btn-block']) : '' ;
                    },
                    'rep' => function($url,$model,$key){
                        return $model->approve ? '<div class="btn btn-success">ผ่านการตรวจสอบ</div>' : '' ;
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {audit}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a(Icon::show('eye'),['view','id' => $model->AN], ['class'=>'btn btn-info']);
                    },
                    'audit' => function($url,$model,$key){
                        $check = CheckAudit::find()->where(['AN' => $model->AN])->one();
                        return $check->AN == null ? Html::a(Icon::show('edit'),['/check-audit/create','AN' => $model->AN], ['class'=>'btn btn-primary']):'';
                    },
                ],
            ],
        ],
    ]); ?>
</div>
