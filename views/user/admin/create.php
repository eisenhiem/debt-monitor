<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use kartik\icons\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap\Nav;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 */

$this->title = 'เพิ่มข้อมูลผู้ใช้';

?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user'),]) ?>


<div class="row">
    <div class="col-lg-offset-3 col-lg-9">
        <div class="card card-default">
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'wrapper' => 'col-sm-9',
                        ],
                    ],
                ]); ?>

                <?= $this->render('_user', ['form' => $form, 'user' => $user]) ?>

                
                    <div class="form-group" style="justify-content: center;">
                        <div class="col-md-8 offset-2 col-sm-10 offset-1">
                            <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-block btn-success']) ?>
                        </div>
                    </div>
                

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>