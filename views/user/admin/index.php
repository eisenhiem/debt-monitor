<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use app\models\Profile;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use \dektrium\user\models\User;


$user = User::find()->all();

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'จัดการผู้ใช้');
?>

<p>
    <?= Html::a(Icon::show('user-plus').' Add User', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<div class="row">
    <?php
    foreach ($user as $u) { ?>

        <div class="col-md-4 col-sm-6 col-12">
            <div class="card text-right">
                <div class="card-body">
                    <div class="card-title" style="font-size: 24pt;color:#1572A1;"><?= $u->username ?></div><br>
                    <div class="card-text"><?= $u->profile->fullname ?><br><?= $u->profile->position ?><br></div>
                </div> 
                <div class="card-footer">
                <?= Html::a(Icon::show('id-card') . ' Profile', ['/profile/update','user_id'=>$u->id], ['class' => 'btn btn-info btn-block']) ?>
                <?= Html::a(Icon::show('key').' User/Pass', ['update','id'=>$u->id], ['class' => 'btn btn-warning btn-block']) ?>
                <?= Html::a(Icon::show('key').' Change Role', ['/role/update','id'=>$u->id], ['class' => 'btn btn-primary btn-block']) ?>
                </div>
            </div>

            <!-- END your <body> content of the Card above this line, right before "Card::end()"  -->
        </div>

    <?php }
    ?>
</div>