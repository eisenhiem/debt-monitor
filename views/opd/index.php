<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\icons\Icon;

if($inscl == 'OFC'){
    $topic = 'ข้าราชการ';
}
if($inscl == 'LGO'){
    $topic = 'อปท.';
}
if($inscl == 'SSS'){
    $topic = 'ประกันสังคม';
}
if($inscl == 'UCS'){
    $topic = 'ค่าใช้จ่ายสูง/COVID';
}

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClaimOpdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Claim OPD';

?>
<div class="claim-opd-index">

    <?php echo $this->render('_search', ['model' => $searchModel,'inscl'=>$inscl]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "รายการเคลม". $topic . ' ' . Html::a(Icon::show('plus'), ['create'], ['class' => 'btn btn-warning']),
            'type' => GridView::TYPE_PRIMARY
        ],
        'floatHeaderOptions' => ['top' => $scrollingTop],
        'rowOptions'=>function($model){
            if($model->getRepStatus() == 'Deny'){
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'claim_id',
            'hn',
            'date_service',
            'time_service',
            [
                'attribute' => 'pttype',
                'value' => function($model){
                    return $model->getPttypename();
                }
            ],
            'doctor_id',
            [
                'header' => 'สถานะ',
                'value' => function($model){
                    if($model->getRepStatus() == 'Deny' && $model->status_id == 1){
                        return 'Appearl';
                    } else if ($model->getRepStatus() && $model->status_id == 2) {
                        return $model->getRepStatus();
                    } else if (!$model->getRepStatus() == 'Deny' && $model->status_id == 1) {
                        return 'Send';
                    }
                    return 'ยังไม่ส่ง';
                }
            ],
            //'income_id',
            //'claim_price',
            //'status_id',
            //'user_id',
            //'d_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions'=>[ 'style'=>'width: 120px'],
                'template' => '{view} {send} {rep} {appearl}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a(Icon::show('eye'), ['view', 'claim_id' => $model->claim_id], ['class' => 'btn btn-info']);
                    },
                    'send' => function ($url, $model, $key) {
                        return $model->status_id == 0 ? Html::a(Icon::show('paper-plane'), ['send', 'claim_id' => $model->claim_id], ['class' => 'btn btn-warning']):'';
                    },
                    'rep' => function ($url, $model, $key) {
                        return $model->status_id == 1 ? Html::a(Icon::show('inbox'), ['rep', 'claim_id' => $model->claim_id], ['class' => 'btn btn-success']):'';
                    },
                    'appearl' => function ($url, $model, $key) {
                        return $model->status_id == 2 && $model->getRepStatus() == 'Deny' ? Html::a(Icon::show('share'), ['appearl', 'claim_id' => $model->claim_id], ['class' => 'btn btn-danger']):'';
                    },
                ],
            ],
        ],
    ]); ?>


</div>
