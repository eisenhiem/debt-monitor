<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpd */

$this->title = $model->claim_id;
\yii\web\YiiAsset::register($this);
?>
<div class="claim-opd-view">

    <p>
        <?= Html::a('Update', ['update', 'claim_id' => $model->claim_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'claim_id' => $model->claim_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'claim_id',
            'hn',
            'date_service',
            'time_service',
            'pttype',
            'income_id',
            'doctor_id',
            'claim_price',
            'status_id',
            'user_id',
            'd_update',
        ],
    ]) ?>

</div>
