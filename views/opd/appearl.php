<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpd */

$this->title = 'Appearl Claim OPD: ' . $model->claim_id;
?>
<div class="claim-opd-update">

    <?= $this->render('_form_appearl', [
        'model' => $model,
    ]) ?>

</div>
