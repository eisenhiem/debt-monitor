<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="claim-opd-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 offset-sm-0">


        <?= $form->field($model, 'appearl_date')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'appearl_date',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่อุธรณ์'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ) ?>

        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>