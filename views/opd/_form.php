<?php

use app\models\Doctors;
use app\models\Pttype;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;


$inscl = [
    'OFC' => 'ข้าราชการ',
    'LGO' => 'อปท.',
    'SSS' => 'ประกันสังคม',
    'UCS' => 'ค่าใช้จ่ายสูง/COVID',
    'UCW' => 'UC Walk in',
    'ANC' => 'ฝากครรภ์',
    'ATK' => 'ATK',
];

$doctor = ArrayHelper::map(Doctors::find()->where(['STATUS' => 1])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME');
$pttype = ArrayHelper::map(Pttype::find()->where(['STATUS' => 1])->asArray()->all(), 'PTTYPE_ID', 'PTTYPE_NAME');
/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="claim-opd-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 offset-sm-0">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'hn')->textInput() ?>
            </div>
            <div class="col-6">

                <?= $form->field($model, 'date_service')->widget(
                    DatePicker::ClassName(),
                    [
                        'name' => 'date_service',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'options' => ['placeholder' => 'ระบุวันที่รับบริการ'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                    ]
                ) ?>
            </div>
            <div class="col-3">

                <?= $form->field($model, 'time_service')->widget(
                    MaskedInput::ClassName(),
                    [
                        'name' => 'time_service',
                        'mask' => '99:99'
                    ]
                ) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-8">
                <?= $form->field($model, 'pttype')->dropDownList($pttype) ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'inscl')->dropDownList($inscl) ?>
            </div>

        </div>
        <div class="row">
            <div class="col-8">
                <?= $form->field($model, 'doctor_id')->dropDownList($doctor) ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'claim_price')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>