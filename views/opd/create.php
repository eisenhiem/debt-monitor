<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpd */

$this->title = 'เพิ่มรายการ Claim OPD';
?>
<div class="claim-opd-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
