<?php

use app\models\Doctors;
use app\models\Pttype;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;


$inscl = [
    'OFC' => 'ข้าราชการ',
    'LGO' => 'อปท.',
    'SSS' => 'ประกันสังคม',
    'UCS' => 'ค่าใช้จ่ายสูง/COVID',
    'ANC' => 'ฝากครรภ์',
    'ATK' => 'ATK',
];

$doctor = ArrayHelper::map(Doctors::find()->where(['STATUS' => 1])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME');
$pttype = ArrayHelper::map(Pttype::find()->where(['STATUS' => 1])->asArray()->all(), 'PTTYPE_ID', 'PTTYPE_NAME');
/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="claim-opd-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-info text-center col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 offset-sm-0">


        <?= $form->field($model, 'rep_date')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'rep_date',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่ตอบกลับ'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        ) ?>
        <?= $form->field($model, 'rep_status')->radioList($model->getItemStatus()) ?>

        <div class="form-group">
            <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>