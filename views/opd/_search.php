<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ClaimOpdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="claim-opd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index','inscl'=>$inscl],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-2">
            <?= $form->field($model, 'hn')->textInput(['placeholder' => 'ระบุ HN ...'])->label(false) ?>
        </div>
        <div class="col-4">
        <?= $form->field($model, 'date_service')->widget(DatePicker::ClassName(),
    [
        'name' => 'date_service', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่รับบริการ'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ])->label(false); ?>
        </div>
        <div class="col-md-2 col-sm-3">
            <div class="form-group">
                <?= Html::submitButton(Icon::show('search') . ' ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php // = $form->field($model, 'pttype') ?>

    <?php // echo $form->field($model, 'income_id') ?>

    <?php // echo $form->field($model, 'doctor_id') ?>

    <?php // echo $form->field($model, 'claim_price') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
