<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\CstatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'สถานะการเคลม';
?>
<div class="cstatus-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการสถานะการเคลม",
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'CLAIM_STATUS',
            'DETAIL',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','id' => $model->CLAIM_STATUS], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
