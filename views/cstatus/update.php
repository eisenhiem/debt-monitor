<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cstatus */

$this->title = 'Update Cstatus: ' . $model->CLAIM_STATUS;
$this->params['breadcrumbs'][] = ['label' => 'Cstatuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CLAIM_STATUS, 'url' => ['view', 'id' => $model->CLAIM_STATUS]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cstatus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
