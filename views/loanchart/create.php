<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Loanchart */

$this->title = 'ยืมเวชระเยียน';

?>
<div class="loanchart-create">

    <?= $this->render('_loan', [
        'model' => $model,
    ]) ?>

</div>
