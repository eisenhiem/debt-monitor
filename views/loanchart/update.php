<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Loanchart */

$this->title = 'คืนเวชระเบียน: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'ยืม/คืน เวชระเบียน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'คืนเวชระเบียน';
?>
<div class="loanchart-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
