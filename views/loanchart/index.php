<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoanchartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทะเบียน ยืม/คืน Chart';

?>
<div class="loanchart-index">
    <font style="font-size:x-large"></font>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการชาร์ต " . Html::a(Icon::show('plus'), ['create'], ['class' => 'btn btn-warning']),
            'type' => GridView::TYPE_PRIMARY
        ],
        'floatHeaderOptions' => ['top' => $scrollingTop],
        'rowOptions'=>function($model){
            if(!$model->RETURN_DATE){
                return ['class' => 'danger'];
            }
            if($model->RETURN_DATE){
                return ['class' => 'warning'];
            }
        },
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'AN',
            'BROUGTH_BY',
            [
                'attribute' => 'BROUGTH_DATE',
                'format' => ['date', 'php:d/m/Y'],
            ],
            'RETURN_BY',
            [
                'attribute' => 'RETURN_DATE',
                'format' => ['date', 'php:d/m/Y'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a(Icon::show('eye'), ['view', 'id' => $model->id], ['class' => 'btn btn-info']);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a(Icon::show('inbox').'คืน', ['update', 'id' => $model->id], ['class' => 'btn btn-success']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>