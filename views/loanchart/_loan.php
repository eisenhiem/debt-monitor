<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\Loanchart */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loanchart-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card card-info text-center col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">

        <?= $form->field($model, 'AN')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'BROUGTH_BY')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'BROUGTH_DATE')->widget(
            DatePicker::ClassName(),
            [
                'name' => 'วันยืม',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'ระบุวันที่ยืมชาร์ต'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]
        );
        ?>

        <div class="form-group">
            <?= Html::submitButton(Icon::show('save').' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>