<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\LoanchartSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loanchart-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<div class="row">
    <div class="col-md-2 col-sm-4 col-xs-6">
    <?= $form->field($model, 'AN')->textInput(['placeholder' => 'ระบุ AN ...'])->label(false) ?>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
    <?= $form->field($model, 'BROUGTH_BY')->textInput(['placeholder' => 'ระบุ ผู้ยืม ...'])->label(false) ?>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
    <?= $form->field($model, 'BROUGTH_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'วันยืม', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่ยืม ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ])->label(false); 
    ?>

    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
    <?= $form->field($model, 'RETURN_BY')->textInput(['placeholder' => 'ระบุ ผู้คืน ...'])->label(false) ?>

    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
    <?= $form->field($model, 'RETURN_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'วันคืน', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่คืน ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ])->label(false); 
    ?>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6">
    <div class="form-group">
    <?= Html::submitButton(Icon::show('search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>       
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
