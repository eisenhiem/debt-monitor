<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel app\models\StatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ตั้งค่าสถานะ';
?>
<div class="status-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการสถานะชาร์ต",
            'type' => GridView::TYPE_PRIMARY
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'STATUS_ID',
            'DETAIL',
            'WORKING_TIME',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','id' => $model->STATUS_ID], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
