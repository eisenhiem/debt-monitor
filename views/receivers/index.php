<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReceiversSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receivers';
?>
<div class="receivers-index">

    <p>
        <?= Html::a(Icon::show('plus').' เพิ่มผู้รับ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายชื่อเจ้าหน้าที่",
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'RECEIVER_NAME',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','id' => $model->RECEIVER_ID], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
