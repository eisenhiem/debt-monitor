<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Receivers */

$this->title = 'เพิ่มเจ้าหน้าที่';
?>
<div class="receivers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
