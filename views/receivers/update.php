<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Receivers */

$this->title = 'แก้ไข ' . $model->RECEIVER_ID;

?>
<div class="receivers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
