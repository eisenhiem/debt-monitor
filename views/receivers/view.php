<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Receivers */

$this->title = $model->RECEIVER_ID;
$this->params['breadcrumbs'][] = ['label' => 'Receivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receivers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->RECEIVER_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->RECEIVER_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'RECEIVER_ID',
            'RECEIVER_NAME',
        ],
    ]) ?>

</div>
