<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dctype */

$this->title = 'Update Dctype: ' . $model->DCTYPE_ID;
$this->params['breadcrumbs'][] = ['label' => 'Dctypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->DCTYPE_ID, 'url' => ['view', 'id' => $model->DCTYPE_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dctype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
