<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DctypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dctypes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dctype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dctype', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'DCTYPE_ID',
            'DC_DETAIL',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
