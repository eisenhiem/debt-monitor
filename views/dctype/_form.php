<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dctype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dctype-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DCTYPE_ID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DC_DETAIL')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
