<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Doctors */

$this->title = 'Update Doctors: ' . $model->DOCTOR_ID;

?>
<div class="doctors-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
