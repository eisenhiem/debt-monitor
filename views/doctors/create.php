<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Doctors */

$this->title = 'เพิ่มแพทย์';
?>
<div class="doctors-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
