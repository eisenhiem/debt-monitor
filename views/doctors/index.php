<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoctorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายชื่อแพทย์';

?>
<div class="doctors-index">

    <p>
        <?= Html::a(Icon::show('plus') . ' เพิ่มแพทย์', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายชื่อแพทย์",
            'type' => GridView::TYPE_PRIMARY
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'DOCTOR_NAME',
            'DOCTOR_ID',
            //            'STATUS',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'STATUS',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        return $model->STATUS == '1' ?
                            Html::a('ACTIVE', ['deactive', 'id' => $model->DOCTOR_ID], ['class' => 'btn btn-primary btn-block']) :
                            Html::a('DEACTIVE', ['active', 'id' => $model->DOCTOR_ID], ['class' => 'btn btn-danger btn-block']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'AUDITER',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        return $model->AUDITER == '1' ?
                            Html::a('ACTIVE', ['deactive', 'id' => $model->DOCTOR_ID], ['class' => 'btn btn-primary btn-block']) :
                            Html::a('DEACTIVE', ['active', 'id' => $model->DOCTOR_ID], ['class' => 'btn btn-danger btn-block']);
                    },
                ]
            ],
        ],
    ]); ?>
</div>