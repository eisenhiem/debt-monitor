<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckAudit */

$this->title = 'บันทึก';
$this->params['breadcrumbs'][] = ['label' => 'การตรวจสอบเวชระเบียนผู้ป่วยใน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-audit-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
