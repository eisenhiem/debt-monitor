<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$answer = [
    '1' => 'ใช่',
    '0' => 'ไม่ใช่',
];

/* @var $this yii\web\View */
/* @var $model app\models\CheckAudit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-audit-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-xs-8 offset-xs-2">
            <div class="card card-primary">
                <div class="card-title text-center bg-success">
                    <b>AN : <?= $model->AN ?></b>
                </div>
                <div class="card-body">
                    <?= $form->field($model, 'is_admission_note')->radioList($answer) ?>
                    
                    <?= $form->field($model, 'is_admit_by_phone')->radioList($answer) ?>

                    <?= $form->field($model, 'is_refer')->radioList($answer) ?>

                    <?= $form->field($model, 'is_consult_by_phone')->radioList($answer) ?>

                    <?= $form->field($model, 'is_sign')->radioList($answer) ?>

                    <?= $form->field($model, 'is_document__complete')->radioList($answer) ?>

                    <?= $form->field($model, 'diagnosis')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'order_time')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>