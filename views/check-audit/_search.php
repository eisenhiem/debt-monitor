<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CheckAuditSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-audit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-2">
    <?= $form->field($model, 'AN')->textInput(['placeholder' => 'ระบุ AN ...'])->label(false) ?>
        </div>

        <div class="col-md-2 col-sm-3">
            <div class="form-group">
                <?= Html::submitButton(Icon::show('search') . ' ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
