<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CheckAuditSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การตรวจสอบเวชระเบียนผู้ป่วยใน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-audit-index">

    <?php echo $this->render('_search', ['model' => $searchModel,'inscl'=>$inscl]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'panel' => [
            'heading' => "การตรวจสอบเวชระเบียนผู้ป่วยใน". $topic,
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'AN',
            [
                'attribute' => 'is_admission_note',
                'value' => function ($model) {
                    return $model->is_admission_note == 1 ? 'ใช่' : 'ไม่ใช่';
                },
            ],
            [
                'attribute' => 'is_admit_by_phone',
                'value' => function ($model) {
                    return $model->is_admit_by_phone == 1 ? 'ใช่' : 'ไม่ใช่';
                },
            ],
            [
                'attribute' => 'is_refer',
                'value' => function ($model) {
                    return $model->is_refer == 1 ? 'ใช่' : 'ไม่ใช่';
                },
            ],
            [
                'attribute' => 'is_consult_by_phone',
                'value' => function ($model) {
                    return $model->is_consult_by_phone == 1 ? 'ใช่' : 'ไม่ใช่';
                },
            ],
            [
                'attribute' => 'is_sign',
                'value' => function ($model) {
                    return $model->is_sign == 1 ? 'ใช่' : 'ไม่ใช่';
                },
            ],
            [
                'attribute' => 'is_document__complete',
                'value' => function ($model) {
                    return $model->is_document__complete == 1 ? 'ใช่' : 'ไม่ใช่';
                },
            ],
            'diagnosis',
            'order_time',
        ],
    ]); ?>


</div>
