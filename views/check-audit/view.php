<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CheckAudit */

$this->title = $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Check Audits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="check-audit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'AN' => $model->AN], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'AN' => $model->AN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'AN',
            'is_admit_by_phone',
            'is_refer',
            'is_consult_by_phone',
            'is_sign',
            'is_document__complete',
            'diagnosis',
            'order_time',
            'd_update',
        ],
    ]) ?>

</div>
