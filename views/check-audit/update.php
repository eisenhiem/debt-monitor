<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CheckAudit */

$this->title = 'แก้ไข: ' . $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'การตรวจสอบเวชระเบียนผู้ป่วยใน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->AN, 'url' => ['view', 'AN' => $model->AN]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="check-audit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
