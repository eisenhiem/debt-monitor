<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="before-audit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'AN')->textInput() ?>

    <?= $form->field($model, 'PDX')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SDX')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADJRW')->textInput() ?>

    <?= $form->field($model, 'DOCUMENT_REF')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'COMMENT')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
