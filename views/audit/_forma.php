<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Doctors;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */
/* @var $form yii\widgets\ActiveForm */

const STATUS_ACTIVE = 1 ;  
const STATUS_DEACTIVE = 0 ; 

$auditer = ArrayHelper::map(Doctors::find()->where(['AUDITER'=>STATUS_ACTIVE])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME');
?>

<div class="after-audit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'AN')->textInput() ?>

    <?php /*= $form->field($model, 'PRINCIPLE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COMOBIT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COMPLICATION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OTHER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'EXTERNAL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADJRW')->textInput() */ ?>

    <?php //= $form->field($model, 'AUDITER_ID')->dropDownList($auditer) ?>

    <?= $form->field($model, 'COMMENT')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
