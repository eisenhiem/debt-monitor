<?php

use yii\helpers\Html;
use app\models\Doctors;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BeforeAuditSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
const STATUS_ACTIVE = 1 ;  
const STATUS_DEACTIVE = 0 ;  

$this->title = 'Audit เวชระเบียนผู้ป่วยใน';

?>
<div class="before-audit-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "รายการชาร์ต",
            'type' => GridView::TYPE_PRIMARY
          ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'AN',
            [
                'attribute'=>'AN',
                //'filter'=>ArrayHelper::map(Doctors::find()->where(['STATUS'=>STATUS_ACTIVE])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME'),
                'label'=>'แพทย์',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDoctorName();
                }
            ],

            'PRINCIPLE',
            /*
            [
                'attribute' => 'COMOBIT',
                'value' => function($model){
                    return $model->COMOBIT != null ? $model->COMOBIT : '';  
                }
            ],
        
            [
                'attribute' => 'COMPLICATION',
                'value' => function($model){
                    return $model->COMPLICATION != null ? $model->COMPLICATION : '';  
                }
            ],
            [
                'attribute' => 'OTHER',
                'value' => function($model){
                    return $model->OTHER != null ? $model->OTHER : '';  
                }
            ],
            [
                'attribute' => 'EXTERNAL',
                'value' => function($model){
                    return $model->EXTERNAL != null ? $model->EXTERNAL : '';  
                }
            ],
            */
            'ADJRW',
            [
                'attribute'=>'AN',
                'label'=>'โรคหลัก หลัง Audit',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getPdx();
                }
            ],
            /*
            [
                'attribute'=>'AN',
                'label'=>'โรคร่วม หลัง Audit',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getSdx();
                }
            ],
            */
            [
                'attribute'=>'AN',
                'label'=>'AdjRW หลัง Audit',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getAuditAdjrw();
                }
            ],
            [
                'attribute'=>'AN',
                'label'=>'',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    if($data->getAuditAdjrw()){
                        $a = $data->getAuditAdjrw()-$data->ADJRW;
                        if($a>0){
                            return '<font style="font-size:xx-large"><span style="color:green">'.Icon::show('caret-up').'</span></font>';
                        }elseif($a<0){
                            return '<font style="font-size:xx-large"><span style="color:red">'.Icon::show('caret-down').'</span></font>';
                        }else{
                            return '<span>'.Icon::show('equals').'</span>';
                        }
                    }
                    return '-';
                }
            ],
            [
                'attribute'=>'AN',
                'label'=>'หมายเหตุ',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getComment();
                }
            ],

            //'DOCUMENT_REF:ntext',
            //'COMMENT:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:140px;'],
                'template'=>'{view} {audit}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a(Icon::show('eye'),['view','id' => $model->AN], ['class'=>'btn btn-info']);
                    },
                    'audit' => function($url,$model,$key){
                        return Html::a(Icon::show('pen'),['audit','id' => $model->AN], ['class'=>'btn btn-success']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
