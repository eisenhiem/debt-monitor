<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */

$this->title = 'บันทึกผล Audit: ' . $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Audit เวชระเบียน', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->AN, 'url' => ['view', 'id' => $model->AN]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="before-audit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_forma', [
        'model' => $model,
    ]) ?>

</div>
