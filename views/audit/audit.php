<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */

$this->title = $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Audit เวชระเบียน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="before-audit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->AN], ['class' => 'btn btn-primary']) ?>
        <?php /*= Html::a('Delete', ['delete', 'id' => $model->AN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'AN',
            'PDX',
            'SDX',
            'ADJRW',
            'DOCUMENT_REF:ntext',
            'COMMENT:ntext',
        ],
    ]) ?>
    </div>
</div>
