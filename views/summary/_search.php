<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;

$status = [0 => 'ยังไม่ D/C',1 => 'D/C แล้วยังไม่ได้รับชาร์ต', 2 => 'ยังไม่ได้ส่งแพทย์สรุปชาร์ต',3 => 'ยังไม่ได้รับสรุปชาร์ต', 4 => 'ยังไม่ได้ส่ง Audit', 5 => 'ยังไม่ได้รับชาร์ต Audit', 6 => 'ยังไม่ได้ Coding', 7 => 'ยังไม่ได้ส่งเคลม' , 8 => 'ยังไม่ได้รับ REP', 'A' => 'Approve ผ่านแล้ว','C' => 'Deny ติด C'];


/* @var $this yii\web\View */
/* @var $model app\models\SummarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="summary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<div class="row">
    <div class="col-3"><?= $form->field($model, 'AN')->textInput(['placeholder' => 'ระบุ AN ...'])->label(false) ?></div>
    <div class="col-3"><?= $form->field($model, 'STATUS_ID')->dropDownList($status, ['prompt'=>'เลือกสถานะ'])->label(false) ?></div>
    <div class="col-2"><?= Html::submitButton(Icon::show('search').' ค้นหา', ['class' => 'btn btn-primary']) ?></div>
</div>

    <?php ActiveForm::end(); ?>

</div>
