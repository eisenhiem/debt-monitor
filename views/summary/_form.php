<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Summary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="summary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'AN')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DOCTOR_ID')->textInput() ?>

    <?= $form->field($model, 'PTTYPE_ID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admit')->textInput() ?>

    <?= $form->field($model, 'dc')->textInput() ?>

    <?= $form->field($model, 'dr_summary')->textInput() ?>

    <?= $form->field($model, 'receive')->textInput() ?>

    <?= $form->field($model, 'eclaim')->textInput() ?>

    <?= $form->field($model, 'approve')->textInput() ?>

    <?= $form->field($model, 'D_UPDATE')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
