<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การติดตามเวชระเบียนผู้ป่วยใน';

?>
<div class="summary-index">

  <?php echo $this->render('_search', ['model' => $searchModel]); ?>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'panel' => [
      'heading' => "รายการชาร์ต",
      'type' => GridView::TYPE_PRIMARY
    ],
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],

      'AN',
      //'DOCTOR_ID',
      //'PTTYPE_ID',
      [ // แสดงข้อมูลออกเป็น icon
        'attribute' => 'admit',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 0 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'attribute' => 'จำหน่าย',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 1 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'attribute' => 'รับชาร์ตจาก IPD',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 2 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'attribute' => 'แพทย์สรุป',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 3 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'attribute' => 'รับสรุปชาร์ต',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 4 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'header' => 'ส่ง Audit',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 5 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'header' => 'รับชาร์ต Audit',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 6 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'header' => 'Coding',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 7 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'header' => 'ส่งเคลม',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->STATUS_ID >= 8 || in_array($model->STATUS_ID,['A','C']) ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'header' => 'สถานะ',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          if ($model->STATUS_ID == '9' && $model->approve) {
            $result = "<span style=\"color:green;\">ผ่านการตรวจสอบ</span>";
          } elseif ($model->STATUS_ID == '9' && !$model->approve) {
            $result = "<span style=\"color:red;\"><b>ติด C Deny</b></span>";
          } elseif ($model->STATUS_ID >= 8) {
            $result = "<span style=\"color:blue;\">รอตอบกลับ</span>";
          } else {
            $result = "<span style=\"color:red;\">ยังไม่ส่ง</span>";
          }
          return $result;
        }
      ],
      [ // แสดงข้อมูลออกเป็น icon
        'header' => 'Rep',
        'headerOptions' => [
          'class' => 'text-center',
        ],
        'contentOptions' => [
          'class' => 'text-center',
        ],
        'format' => 'html',
        'value' => function ($model, $key, $index, $column) {
          return $model->approve ? "<span style=\"color:green;\">" . Icon::show('check') . "</span>" : "<span style=\"color:red;\">" . Icon::show('times') . "</span>";
        }
      ],

      //'admit',
      //'dc',
      //'dr_summary',
      //'receive',
      //'eclaim',
      //'approve',
      //'D_UPDATE',

      // ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>
</div>