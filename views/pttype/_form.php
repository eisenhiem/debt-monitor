<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pttype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pttype-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-xs-8 offset-xs-2">
            <div class="card card-primary">
                <div class="card-body">
                    <?= $form->field($model, 'PTTYPE_ID')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'PTTYPE_NAME')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'STATUS')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>