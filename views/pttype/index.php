<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PttypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ตั้งค่าสิทธิ์';

?>
<div class="pttype-index">

    <p>
        <?= Html::a(Icon::show('plus') . ' เพิ่มสิทธิ์', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการสิทธิ์",
            'type' => GridView::TYPE_PRIMARY
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PTTYPE_NAME',
            'PTTYPE_ID',
            //'STATUS',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'STATUS',
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        return $model->STATUS == '1' ?
                            Html::a('<span style="color:green" class="glyphicon glyphicon-ok"> ACTIVE </span>', ['deactive', 'id' => $model->PTTYPE_ID]) :
                            Html::a('<span style="color:red" class="glyphicon glyphicon-remove"> DEACTIVE </span>', ['active', 'id' => $model->PTTYPE_ID]);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','id' => $model->PTTYPE_ID], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>