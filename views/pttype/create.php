<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pttype */

$this->title = 'เพิ่มสิทธิ์';
?>
<div class="pttype-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
