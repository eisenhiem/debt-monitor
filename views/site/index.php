<?php

use app\models\Charge;
use app\models\ClaimOpd;

$this->title = 'Dashboard';

$admit = Charge::find()->count();
$dc = Charge::find()->where('STATUS_ID > 0')->count();
$dc_rate = floor($dc / $admit * 100) . '%';
$claim = Charge::find()->where('STATUS_ID >= 5')->count();
$claim_rate = floor($claim / $dc * 100) . '%';
$approve = Charge::find()->joinWith('approve')->where('STATUS_ID = "6"')->count();
$deny = Charge::find()->where('STATUS_ID >= "C"')->count();
if($claim){
    $approve_rate = floor($approve / $claim * 100) . '%';
    $deny_rate = floor($deny / $claim * 100) . '%';
} else {
    $approve_rate = '0%';
    $deny_rate = '0%';
}

$sss = ClaimOpd::find()->where(['inscl' => 'SSS'])->count();
$sss_claim = ClaimOpd::find()->where(['inscl' => 'SSS'])->andWhere('status_id >= 1')->count();
if($sss){
    $sss_claim_rate = floor($sss_claim / $sss * 100) . '%';
} else {
    $sss_claim_rate = '0%';
}

$ofc = ClaimOpd::find()->where(['inscl' => 'OFC'])->count();
$ofc_claim = ClaimOpd::find()->where(['inscl' => 'OFC'])->andWhere('status_id >= 1')->count();

$lgo = ClaimOpd::find()->where(['inscl' => 'LGO'])->count();
$lgo_claim = ClaimOpd::find()->where(['inscl' => 'LGO'])->andWhere('status_id >= 1')->count();
if($ofc || $lgo) {
    $ofc_lgo_claim_rate = floor(($ofc_claim + $lgo_claim) / ($ofc + $lgo) * 100) . '%';
} else {
    $ofc_lgo_claim_rate = '0%';
}

$ucs = ClaimOpd::find()->where(['inscl' => 'UCS'])->count();
$ucs_claim = ClaimOpd::find()->where(['inscl' => 'UCS'])->andWhere('status_id >= 1')->count();
if($ucs){
    $ucs_claim_rate = floor($ucs_claim / $ucs * 100) . '%';
} else {
    $ucs_claim_rate = '0%';

}    


$anc = ClaimOpd::find()->where(['inscl' => 'ANC'])->count();
$anc_claim = ClaimOpd::find()->where(['inscl' => 'ANC'])->andWhere('status_id >= 1')->count();
$anc_claim_rate = $anc == 0 ? 0:floor($anc_claim / $anc * 100) . '%';

?>

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <b>IPD</b>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'Admit',
                        'number' => number_format($admit),
                        'icon' => 'fas fa-bed',
                        'theme' => 'primary',
                        'progress' => [
                            'width' => $dc_rate,
                            'description' => 'จำหน่ายแล้ว ' . number_format($dc),
                        ]
                    ]) ?>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'Claim',
                        'number' => number_format($claim),
                        'icon' => 'fas fa-file-export',
                        'theme' => 'info',
                        'progress' => [
                            'width' => $claim_rate,
                            'description' => 'เคลมไปแล้ว ' . $claim_rate,
                        ]
                    ]) ?>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'Approve',
                        'number' => number_format($approve),
                        'icon' => 'far fa-check-circle',
                        'theme' => 'success',
                        'progress' => [
                            'width' => $approve_rate,
                            'description' => 'การส่งเคลมผ่าน ' . $approve_rate,
                        ]
                    ]) ?>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'Deny',
                        'number' => number_format($deny),
                        'icon' => 'far fa-times-circle',
                        'theme' => 'danger',
                        'progress' => [
                            'width' => $deny_rate,
                            'description' => 'ติด C Deny ' . $deny_rate
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <b>OPD</b>
        </div>
        <div class="card-body">
        <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'ข้าราชการ/อปท.',
                        'number' => number_format($ofc + $lgo),
                        'icon' => 'fas fa-bed',
                        'theme' => 'primary',
                        'progress' => [
                            'width' => $ofc_lgo_claim_rate,
                            'description' => 'เคลมไปแล้ว ' . $ofc_lgo_claim_rate ,
                        ]
                    ]) ?>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'ประกันสังคม',
                        'number' => number_format($sss),
                        'icon' => 'fas fa-file-export',
                        'theme' => 'info',
                        'progress' => [
                            'width' => $sss_claim_rate,
                            'description' => 'เคลมไปแล้ว ' . $sss_claim_rate,
                        ]
                    ]) ?>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'UCS',
                        'number' => number_format($ucs),
                        'icon' => 'far fa-check-circle',
                        'theme' => 'success',
                        'progress' => [
                            'width' => $ucs_claim_rate,
                            'description' => 'การส่งเคลมผ่าน ' . $ucs_claim_rate,
                        ]
                    ]) ?>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <?= \hail812\adminlte\widgets\InfoBox::widget([
                        'text' => 'Fee Schedule',
                        'number' => number_format($anc),
                        'icon' => 'far fa-times-circle',
                        'theme' => 'danger',
                        'progress' => [
                            'width' => $anc_claim_rate,
                            'description' => 'การส่งเคลมผ่าน ' . $anc_claim_rate
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
