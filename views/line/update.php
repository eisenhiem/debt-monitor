<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Line */

$this->title = 'แก้ไข';

?>
<div class="line-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
