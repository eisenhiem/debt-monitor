<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Line */

$this->title = $model->line_id;

\yii\web\YiiAsset::register($this);
?>
<div class="line-view">

    <p>
        <?= Html::a('Update', ['update', 'line_id' => $model->line_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'line_id' => $model->line_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'line_id',
            'line_group',
            'line_token:ntext',
            'line_status',
        ],
    ]) ?>

</div>
