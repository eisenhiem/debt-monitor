<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;



/* @var $this yii\web\View */
/* @var $searchModel app\models\LineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ตั้งค่า Line notify';
?>
<div class="line-index">

    <p>
        <?= Html::a(Icon::show('plus').' เพิ่มกลุ่ม Line', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "กลุ่ม Line Notify",
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'line_id',
            'line_group',
            //'line_token:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'สถานะ',
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        return $model->line_status == '1' ?
                            Html::a('<span style="color:green" class="glyphicon glyphicon-ok"> ACTIVE </span>', ['deactive', 'id' => $model->line_id]) :
                            Html::a('<span style="color:red" class="glyphicon glyphicon-remove"> DEACTIVE </span>', ['active', 'id' => $model->line_id]);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','id' => $model->line_id], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
