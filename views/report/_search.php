<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model app\models\ReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-4">
        <?= $form->field($model, 'dc_month')->widget(DatePicker::ClassName(),
    [
        'name' => 'dc_month', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุเดือนที่ DC'],
        'pluginOptions' => [
            'format' => 'yyyy-mm',
            'todayHighlight' => true
        ]
    ])->label(false); 
    ?>
        </div>
        <div class="col-4">
        <div class="form-group">
        <?= Html::submitButton(Icon::show('search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>
        </div>
    </div>
    <?php // $form->field($model, 'AN') ?>

    <?php //$form->field($model, 'PTTYPE_ID') ?>

    <?php // echo $form->field($model, 'DOCTOR_ID') ?>


    <?php ActiveForm::end(); ?>

</div>
