<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานการเคลม';

?>
<div class="report-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => "รายการชาร์ต ",
            'type' => GridView::TYPE_PRIMARY
        ],
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'ชาร์ต', 'options' => ['colspan' => 3, 'class' => 'text-center info']],
                    ['content' => 'วันที่', 'options' => ['colspan' => 7, 'class' => 'text-center warning']],
                    ['content' => 'ก่อน Audit', 'options' => ['colspan' => 2, 'class' => 'text-center danger']],
                    ['content' => 'หลัง Audit', 'options' => ['colspan' => 3, 'class' => 'text-center danger']],
                ],
            ]
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'AN',
            'PTTYPE_ID',
            [
                'attribute' => 'admit',
                'format' => ['date', 'php:d/m/Y'],
            ],
            [
                'attribute' => 'DC',
                'format' => ['date', 'php:d/m/Y'],
            ],            
            [
                'attribute' => 'receive',
                'format' => ['date', 'php:d/m/Y'],
            ],            
            [
                'attribute' => 'send_dr',
                'format' => ['date', 'php:d/m/Y'],
            ],            
            [
                'attribute' => 'receive_dr',
                'format' => ['date', 'php:d/m/Y'],
            ],            
            [
                'attribute' => 'claim',
                'format' => ['date', 'php:d/m/Y'],
            ],
            [
                'attribute' => 'approve',
                'format' => ['date', 'php:d/m/Y'],
            ],
            'PRINCIPLE',
            //'COMOBIT',
            //'COMPLICATION',
            //'OTHER',
            //'EXTERNAL',
            'ADJRW',
            'after_Pdx',
            //'after_Comob',
            //'after_comp',
            //'after_other',
            //'after_ext',
            'after_adjRW',
            //'dr_name',
            //'DOCTOR_ID',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
