<?php

namespace app\controllers;

use app\models\Appearl;
use app\models\ClaimOpd;
use app\models\ClaimOpdSearch;
use app\models\ClaimRep;
use app\models\ClaimSend;
use DateTime;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OpdController implements the CRUD actions for ClaimOpd model.
 */
class OpdController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ClaimOpd models.
     *
     * @return string
     */
    public function actionIndex($inscl)
    {
        $searchModel = new ClaimOpdSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['inscl' => $inscl]);
        $dataProvider->query->orderBy(['date_service'=>SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'inscl' => $inscl,
        ]);
    }

    /**
     * Displays a single ClaimOpd model.
     * @param int $claim_id Claim ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($claim_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($claim_id),
        ]);
    }

    /**
     * Creates a new ClaimOpd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ClaimOpd();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'claim_id' => $model->claim_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClaimOpd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $claim_id Claim ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($claim_id)
    {
        $model = $this->findModel($claim_id);
        $model->user_id = Yii::$app->user->identity->id;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'claim_id' => $model->claim_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionSend($claim_id)
    {
        $date = new DateTime();
        $d = $date->format('Y-m-d');
        $t= $date->format('H:i');
        $model = new ClaimSend();
        $model->claim_id = $claim_id;
        $model->send_date = $d;
        $model->send_time = $t;
        $model->user_id = Yii::$app->user->identity->id;
        if($model->save()){
            $opd = $this->findModel($claim_id);
            $opd->status_id = 1;
            $opd->save();
            return $this->redirect(['index','inscl' => $opd->inscl]);
        }
    }

    public function actionRep($claim_id)
    {
        $date = new DateTime();
        $d = $date->format('Y-m-d');
        $t= $date->format('H:i');
        $model = ClaimRep::find()->where(['claim_id'=>$claim_id])->one();
        if(!$model){
            $model = new ClaimRep();
            $model->claim_id = $claim_id;
            $model->rep_date = $d;
        }

        if ($this->request->isPost && $model->load($this->request->post())) {
            if($model->save()){
                $opd = $this->findModel($claim_id);
                $opd->status_id = 2;
                $opd->save();
                return $this->redirect(['index','inscl' => $opd->inscl]);
            }
        }

        return $this->render('rep', [
            'model' => $model,
        ]);

    }

    public function actionAppearl($claim_id)
    {
        $date = new DateTime();
        $d = $date->format('Y-m-d');
        $t= $date->format('H:i');
        $model = new Appearl();
        $model->claim_id = $claim_id;
        $model->appearl_date = $d;
        $model->user_id = Yii::$app->user->identity->id;

        if ($this->request->isPost && $model->load($this->request->post())) {
            if($model->save()){
                $opd = $this->findModel($claim_id);
                $opd->status_id = 1;
                $opd->save();
                return $this->redirect(['index','inscl' => $opd->inscl]);
            }
        }

        return $this->render('appearl', [
            'model' => $model,
        ]);

    }
    /**
     * Deletes an existing ClaimOpd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $claim_id Claim ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($claim_id)
    {
        $this->findModel($claim_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClaimOpd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $claim_id Claim ID
     * @return ClaimOpd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($claim_id)
    {
        if (($model = ClaimOpd::findOne(['claim_id' => $claim_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
