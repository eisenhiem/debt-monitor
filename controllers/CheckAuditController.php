<?php

namespace app\controllers;

use app\models\CheckAudit;
use app\models\CheckAuditSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CheckAuditController implements the CRUD actions for CheckAudit model.
 */
class CheckAuditController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all CheckAudit models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CheckAuditSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CheckAudit model.
     * @param int $AN An
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($AN)
    {
        return $this->render('view', [
            'model' => $this->findModel($AN),
        ]);
    }

    /**
     * Creates a new CheckAudit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($AN)
    {
        $model = CheckAudit::findOne(['AN' => $AN]);
        if ($model) {
            return $this->redirect(['update', 'AN' => $model->AN]);
        }
        $model = new CheckAudit();
        $model->AN = $AN;

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'AN' => $model->AN]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CheckAudit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $AN An
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($AN)
    {
        $model = $this->findModel($AN);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'AN' => $model->AN]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CheckAudit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $AN An
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($AN)
    {
        $this->findModel($AN)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CheckAudit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $AN An
     * @return CheckAudit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($AN)
    {
        if (($model = CheckAudit::findOne(['AN' => $AN])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
