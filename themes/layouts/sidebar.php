<?php

use kartik\icons\Icon;
Icon::map($this);

?>

<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <div style="text-align: center;background-color:#396EB0;">
        <a href="./" class="brand-link">
            <!-- <img src="@web/images/logo.jpg" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
            <span class="brand-text font-weight-bold" style="color: white;">Debt Monitor V.2</span>
        </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <?php if (!Yii::$app->user->isGuest) {
            echo '<div class="user-panel mt-3 pb-3 mb-3 d-flex">';
            // echo '<div class="image">';
            // echo '<img src="' . $assetDir . '/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">';
            // echo '</div>';
            echo '<div class="info">';
            echo '<a href="#" class="d-block">User: <b>' . Yii::$app->user->identity->username . '</b></a>';
            echo '</div>';
            echo '</div>';
        } ?>

        <!-- SidebarSearch Form -->
        <!-- href be escaped -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    ['label' => 'MENU', 'header' => true,'visible' => !Yii::$app->user->isGuest,],
                    [
                        'label' => 'Chart IPD Monitor', 'icon' => 'id-badge', 'iconClassAdded' => 'text-danger','visible' => !Yii::$app->user->isGuest,
                        'items' => [

                            ['label' => 'Chart Monitor', 'icon' => 'th','url'=> ['/summary/index']],
                            ['label' => 'Chart Manage', 'icon' => 'list','url'=> ['/charge/index']],
                            ['label' => 'Chart ทะเบียน ยืม/คืน', 'icon' => 'clipboard','url'=> ['/loanchart/index']],
                            ['label' => 'Audit Chart', 'icon' => 'clipboard-check','url'=> ['/audit/index']],
                        ]
                    ],
                    [
                        'label' => 'OPD Monitor', 'icon' => 'id-badge', 'iconClassAdded' => 'text-danger','visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'เคลมข้าราชการ', 'icon' => 'dot-circle','url'=> ['/opd/index','inscl' => 'OFC']],
                            ['label' => 'เคลมอปท.', 'icon' => 'dot-circle','url'=> ['/opd/index','inscl' => 'LGO']],
                            ['label' => 'เคลมประกันสังคม', 'icon' => 'dot-circle','url'=> ['/opd/index','inscl' => 'SSS']],
                            ['label' => 'เคลมค่าใช้จ่ายสูง/COVID', 'icon' => 'dot-circle','url'=> ['/opd/index','inscl' => 'UCS']],
                            ['label' => 'เคลม UC Walk In', 'icon' => 'dot-circle','url'=> ['/opd/index','inscl' => 'UCW']],
                        ]
                    ],
                    [
                        'label' => 'Fee Schedule Monitor', 'icon' => 'id-badge', 'iconClassAdded' => 'text-danger','visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'เคลม ANC', 'icon' => 'female','url'=> ['/opd/index','inscl' => 'ANC']],
                        ]
                    ],
                    [
                        'label' => 'Report', 'icon' => 'list', 'iconClassAdded' => 'text-danger','visible' => !Yii::$app->user->isGuest,
                        'items' => [
                            ['label' => 'รายงาน IPD', 'icon' => 'clipboard-list','url'=> ['/report/index']],
                            ['label' => 'Audit Chart', 'icon' => 'clipboard-check','url'=> ['/check-audit/index']],
                            ['label' => 'รายงาน OPD', 'icon' => 'dot-circle','url'=> ['/']],
                            ['label' => 'รายงาน Fee Schedule', 'icon' => 'dot-circle','url'=> ['/']],
                        ]
                    ],
                    ['label' => 'Admin Menu', 'header' => true,'visible' => Yii::$app->user->identity->role == 1,],
                    [
                        'label' => 'ตั้งค่า',
                        'icon' => 'cogs',
                        'badge' => '<span class="right badge badge-info">2</span>',
                        'visible' => Yii::$app->user->identity->role == 1,
                        'items' => [
                            ['label' => 'สถานะชาร์ต', 'url' => ['/status/index'], 'icon' => 'users',],
                            ['label' => 'สิทธิ์', 'url' => ['/pttype/index'], 'icon' => 'users',],
                            ['label' => 'แพทย์', 'url' => ['/doctors/index'], 'icon' => 'users',],
                            ['label' => 'ประเภทการจำหน่าย', 'url' => ['/dctype/index'], 'icon' => 'users',],
                            ['label' => 'ผู้รับชาร์ต', 'url' => ['/receivers/index'], 'icon' => 'users',],
                            ['label' => 'สถานะ Claim', 'url' => ['/cstatus/index'], 'icon' => 'users',],
                            ['label' => 'ตั้งค่า Line Notify', 'url' => ['/line/index'], 'icon' => 'users',],
                            ['label' => 'จัดการผู้ใช้', 'url' => ['/user/admin/index'], 'icon' => 'users',],
                            ['label' => 'Gii',  'icon' => 'file-code', 'url' => ['/gii'], 'target' => '_blank','iconClassAdded' => 'text-warning'],
                            ]
                    ],
                    ['label' => 'Login', 'url' => ['/user/security/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'url' => ['/user/security/logout'], 'icon' => 'sign-out-alt', 'visible' => !Yii::$app->user->isGuest,'template'=>'&emsp;<a href="{url}" data-method="post">{icon} Log out</a>'],
                    //['label' => 'Debug', 'icon' => 'bug', 'url' => ['/debug'], 'target' => '_blank'],
                    /*                    ['label' => 'MULTI LEVEL EXAMPLE', 'header' => true],
                    ['label' => 'Level1'],
                    [
                        'label' => 'Level1',
                        'items' => [
                            ['label' => 'Level2', 'iconStyle' => 'far'],
                            [
                                'label' => 'Level2',
                                'iconStyle' => 'far',
                                'items' => [
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle'],
                                    ['label' => 'Level3', 'iconStyle' => 'far', 'icon' => 'dot-circle']
                                ]
                            ],
                            ['label' => 'Level2', 'iconStyle' => 'far']
                        ]
                    ],
                    ['label' => 'Level1'],
                    ['label' => 'LABELS', 'header' => true],
                    ['label' => 'Important', 'iconStyle' => 'far', 'iconClassAdded' => 'text-danger'],
                    ['label' => 'Warning', 'iconClass' => 'nav-icon far fa-circle text-warning'],
                    ['label' => 'Informational', 'iconStyle' => 'far', 'iconClassAdded' => 'text-info'],
*/
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>